# Viewing Application
The code in the repository ([https://gitlab.com/devhashtag/webtech](https://gitlab.com/devhashtag/webtech)) is automatically compiled and deployed to [dbl.rvsit.nl](https://dbl.rvsit.nl). Here you can view and test the application.
Source code can be found in this repository and the commits show who created what code.

### Project Members

* @DaMasterM - Michel van de Looij
* @gijs_deman - Gijs de Man
* @Loncelat - Martijn Leus
* @royke - Roy van Schaijk
* @vincentmoonen - Vincent Moonen
* @devhashtag - Peter Heijstek

# Basics

* Install [NodeJS](https://nodejs.org/en/) (LTS version).
* Restart your computer.
* Clone this repository using git, for simplicity install [GitKraken](https://www.gitkraken.com/download-stand-alone)
* Regularly open Gitkraken and click on the pull button (or `git pull`), this gets the most recent commit from this repository.
* If you made changes, click the *// WIP* row, click *Stage all changes*, enter a **descriptive** commit message and *commit*, then *push* your changes to this repository.
* Anything not working? Ask in the *#quick-help* channel of the Discord to avoid clutter in Whatsapp.
* If `package-lock.json` has changes, you can safely commit them, no need to specify why.

# Frontend
## Setup
* Navigate to the *frontend* folder in the project you just cloned, and open a terminal.
* Run `npm install`
## Running
* Run `npm start` in the terminal while in the *frontend* folder, keep this open and use `CTRL-C`(2x) to close when done developing.

# Backend
## Setup
* Navigate to the *backend* folder in the project you just cloned, and open a terminal.
* Run `npm install`
## Running
* Run `npm start` in the terminal while in the *backend* folder, keep this open and use `CTRL-C`(2x) to close when backend is not needed anymore.

# Shorthands
If using Windows, you can also run the `update.bat` and `start.bat` files in the home directory.

# Using the application
To upload a dataset, you can click on 'browse', and then on upload. After it is done with parsing, the uploaded dataset will appear in the list of available datasets (above the upload button).

After a dataset is chosen, a green button will appear. If you click on it, the visualizations will be shown.

On the top-left there is a small menu with four buttons:
 The top-left button lets you go back to the homescreen to select a different dataset
 The top-right button opens a settings menu, where you can adjust some settings for the visualizations
 The bottom-left toggles the node-link diagram on/off
 The bottom-right toggles the heatmap on/off

Graph:
	To select edges in the node-link diagram, you can click on a node. All the edges that go to or come from that node, will then be highlighted (also in the heatmap)
	Zooming and panning is possible

Heatmap:
	To select edges in the heatmap, you can right-click to select a rectangle. All the edges in that rectangle will then be highlighted (also in the node-link diagram)
	Zooming and panning is possible

Settings menu:
	In the settings menu is a slider, which lets you choose a range of weights. The node-link diagram will only show the edges that have a weight in that range.

Supported formats:
	- standard format of given datasets
	- format as described here: https://snap.stanford.edu/data/soc-sign-bitcoin-otc.html