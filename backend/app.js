const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');

const datasetController = require('./src/controllers/datasetController');
const canvasController = require('./src/controllers/canvasController');

//use a different port, 8000 is already taken on my server
const server = app.listen(35264, () => {
    console.log('Server up and running on port 35264');
});

// Allow cross origin requests
app.all('/*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
});

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(bodyParser.json({limit: '0.5mb'}));
app.use(bodyParser.urlencoded({extended: false}));

app.use('/datasets', datasetController);
app.use('/canvas', canvasController);
