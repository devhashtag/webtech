import sys
import json
import math
import numpy as np
from numpy import linalg

fileLoc = sys.argv[1]
clusterAmount = int(sys.argv[2])

# Open the file
with open(fileLoc, 'r+') as file:

    # Decode it as json
    graph = json.load(file)

    # Fetch the 'data' object
    nodes = graph['data']

    # Count the nodes
    numNodes = len(nodes)

    # Create the matrix with zeros
    matrix = [[0 for i in range(numNodes)] for j in range(numNodes)]

    # Fill the matrix with the actual values
    for i in range(numNodes):
        node = nodes[i]
        for key, value in node['outgoingEdges'].items():
            matrix[i][int(key)] = value

    matrix = np.add(matrix, np.transpose(matrix))

    # Add degrees on diagonal
    for i in range(numNodes):
        matrix[i][i] = np.sum(matrix[i])

    # Make Laplacian by substracting everything from the degree matrix
    for i in range(numNodes):
        for j in range(numNodes):
            if i != j:
                matrix[i][j] *= -1

    # Get the lambda en vector pairs
    values, vectors = linalg.eigh(matrix)

    # Get the 2nd vector
    fiedlerVector = vectors[1].tolist()
    fiedlerVectorB = vectors[1].tolist()

    # Create cluster matrix
    clusterArray = [0 for i in range(numNodes)]

    # Assign clusters
    for cluster in range(clusterAmount):
        nodesInCluster = 0
        # Track the number of nodes in each cluster while forming it
        while nodesInCluster < numNodes / clusterAmount:
            min = float("inf")
            minIndex = 0
            # Loop through the Fiedler vector
            for i in range(len(fiedlerVector)):
                # Take the minimum
                if fiedlerVectorB[i] != 'x' and fiedlerVector[i] < min:
                    min = fiedlerVector[i]
                    minIndex = i
            # Make the minimum element unelligable for search again
            fiedlerVectorB[minIndex] = 'x'
            # Assign the cluster
            clusterArray[minIndex] = cluster
            # Track node count in cluster
            nodesInCluster += 1

    # Calculate node count in every cluster
    clusterCounts = [0 for i in range(clusterAmount)]
    for i in range(clusterAmount):
        for j in range(numNodes):
            if clusterArray[j] == i:
                clusterCounts[i] += 1

    # Writing result to json
    for nodeIndex in range(numNodes):
        graph['data'][nodeIndex]['cluster'] = clusterArray[nodeIndex] if numNodes > 5 else 0
    graph['clusterSizes'] = clusterCounts if numNodes > 5 else [numNodes, 0, 0, 0, 0]
    file.seek(0, 0)
    json.dump(graph, file)
