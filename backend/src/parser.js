const fs = require('fs');
const path = require('path');
const reordering = require('./reordering');
const CsvSourceTargetWeight = require('./dataformats/CsvSourceTargetWeight');
const {exec} = require('child_process');
const Papa = require('papaparse');

/**
 * Parses a csv file and saves json
 *
 * @param {string} file The filename of the file that will be parsed
 * @param {string} filename The name of the json-file that is created by this function
 * @param {function} callback callback function that will be called on error or on succes
 */

/* This is the layout of the dictionary (the goal):
  nodes: {
    "name" : the name of the vertex as a string
    "nodeId" : string with node id "n" + int
    "circleX" : the x coordinate in the circle layout, integer
    "circleY" : the y coordinate in the circle layout, integer
    "cuthillMcKeeIndex" : integer of index ordering
    "outgoingEdges" : list with id's of outgoing edges
    "incomingEdges" : list with id's of incoming edges
    "outdegree" : outdegree of the vertex (sum of all weights) float
    "indegree" : indegree of the vertex (sum of all weights) float
  }
*/

function parse(file, filename, callback) {

// ---------------------- IMPORT/READ THE FILE ----------------------------
  fs.readFile(file, (errRead, contents) => {
    let disablePagerank;
    let maxout;
    let zero;
    let sum;
    let biggest;
    let mean;
    let matrix;
    let nodes;
    // Check for read errors
    if (errRead) {
      return callback(errRead);
    }

    // Check for empty file
    if (!contents) {
      return callback(`No file contents from file ${file}`);
    }

    // Convert File contents to string for easy manipulation
    contents = contents.toString();

    //csv with SOURCE, TARGET entries
    if (contents.startsWith('SOURCE')) {
      const PapaData = Papa.parse(contents);

      // Check data format
      const headerRow = PapaData.data.shift();
      if (headerRow[0].trim() !== 'SOURCE' || headerRow[1].trim() !== 'TARGET') {
        throw 'Parsing failed, invalid format';
      }

      // Detect weighted graph
      const weighted = typeof headerRow[2] === 'string' && headerRow[2].trim().length > 0;

      maxout = 0;
      biggest = 0;
      sum = 0;
      zero = 0;
      nodes = [];
      let nameToNodeId = {};

      for (const row of PapaData.data) {
        let row0 = typeof row[0] === 'string' ? row[0].trim() : row[0];
        let row1 = typeof row[1] === 'string' ? row[1].trim() : row[1];
        let row2 = typeof row[2] === 'string' ? row[2].trim() : row[2];
        let sourceIndex = nameToNodeId[row0];
        if (sourceIndex === undefined) {
          sourceIndex = nodes.push({
            name: row0,
            outgoingEdges: {},
            incomingEdges: {},
            outdegree: 0,
            indegree: 0,
          }) - 1;
          nameToNodeId[row0] = sourceIndex;
        }
        const sourceNode = nodes[sourceIndex];

        let targetIndex = nameToNodeId[row1];
        if (targetIndex === undefined) {
          targetIndex = nodes.push({
            name: row1,
            outgoingEdges: {},
            incomingEdges: {},
            outdegree: 0,
            indegree: 0,
          }) - 1;
          nameToNodeId[row1] = targetIndex;
        }
        const targetNode = nodes[targetIndex];

        sourceNode.outgoingEdges[targetIndex] = weighted ? row2 : 1;
        sourceNode.outdegree += 1;
        targetNode.incomingEdges[sourceIndex] = weighted ? row2 : 1;
        targetNode.indegree += 1;

        sum += weighted ? row2 : 1;

        if ((weighted ? row2 : 1) > biggest) {
          biggest = weighted ? row2 : 1;
        }

        if (sourceNode.outdegree > maxout) {
          maxout = sourceNode.outdegree;
        }
      }

      mean = sum / ((nodes.length * nodes.length) - zero);

      matrix = [];
      for (let i = 0; i < nodes.length; i++) {
        const row = [];

        for (let i = 0; i < nodes.length; i++) {
          row.push(0);
        }

        for (let i2 in nodes[i].outgoingEdges) {
          row[i2] = nodes[i].outgoingEdges[i2];
        }

        matrix.push(row);
      }

      //Disable pagerank for this datatype untill fixed
      disablePagerank = true;

    } else {

      // ---------------------- CREATE THE DICTIONARY ----------------------------
      // split on newlines
      const lines = contents.split('\n');

      // shift returns the first element (row of author-names), and we create an array of those names
      let columns = lines.shift().split(';');

      // remove first element (it is empty)
      columns.shift();

      // create objects from the names
      nodes = columns.map((name) => ({name: name.trim(), values: []}));

      // Add the "values" field
      biggest = 0;
      sum = 0;
      zero = 0;

      for (let node of nodes) {
        let values = lines.shift().split(';');
        values.shift();

        if (values[values.length - 1].trim() === '') {
          values.pop();
        }
        node.values = values.map(val => parseFloat(val));

        for (const value of node.values) {
          if (biggest < value) {
            biggest = value;
          }

          if (value !== 0) {
            sum += value;
          } else {
            zero++;
          }
        }
      }

      mean = sum / ((nodes.length * nodes.length) - zero);

      // Make matrix
      matrix = [];
      for (let row in nodes) {
        let node = nodes[row];
        matrix[row] = node.values;
      }

      // // scale the "values" field to [0,1]
      // for (let node of nodes) {
      //     node.values = node.values.map(val => val / biggest);
      //     console.log(node.values)
      // }

      maxout = 0;

      // Add edges
      for (let index in nodes) {
        let node = nodes[index];
        let outdegree = 0;
        let indegree = 0;
        node.outgoingEdges = {};
        node.incomingEdges = {};
        for (let i in matrix[index]) {
          outdegree += matrix[index][i];
          indegree += matrix[i][index];
          if (matrix[index][i] > 0) {
            node.outgoingEdges[i] = matrix[index][i];
          }
          if (matrix[i][index] > 0) {
            node.incomingEdges[i] = matrix[i][index];
          }
        }
        node.outdegree = outdegree;
        node.indegree = indegree;

        if (node.outdegree > maxout) {
          maxout = node.outdegree;
        }

        delete node.values;
      }

    }

    // Add node id, circleX and circleY
    const datasetSize = nodes.length;
    for (let index in nodes) {
      let node = nodes[index];
      node.nodeId = index;
      node.circleX = Math.cos(index * (2 * Math.PI / datasetSize));
      node.circleY = Math.sin(index * (2 * Math.PI / datasetSize));
    }

    /*
    // Get Cuthill McKee reordering
    let cuthillMcKeeReordering = reordering.cuthillMcKee(nodes);
    for (let ogIndex in nodes) {
        let node = nodes[ogIndex];
        let newIndex;
        for (let i in cuthillMcKeeReordering) {
            if (cuthillMcKeeReordering[i] == ogIndex) {
                newIndex = i;
            }
        }
        node.cuthillMcKeeIndex = parseInt(newIndex);
    }
    */

    let symmetric = true;

    /* Symmetric */
    outerloop:
      for (let row = 0; row < matrix.length; row++) {
        for (let column = 0; column < matrix.length; column++) {
          if (matrix[row][column] !== matrix[column][row]) {
            symmetric = false;
            break outerloop
          }
        }
      }

      let disableHierarchical = true;

      let slinkLookup, slinkParents, slinkHeights, slinkMinHeight;
      let clinkLookup, clinkParents, clinkHeights, clinkMinHeight;

      /* Only compute distance matrix for 'small' matrices. */
      if (matrix.length <= 1500) {
        /* Compute the distance matrix. */
        const dissmatrix = reordering.dissimilarityMatrix(matrix, 0);

        /* Apply single-linkage clustering to the distance matrix. */

        [slinkLookup, slinkParents, slinkHeights, slinkMinHeight] = reordering.slink(dissmatrix);

        /* Apply complete-linkage clustering to the distance matrix. */
        [clinkLookup, clinkParents, clinkHeights, clinkMinHeight] = reordering.clink(dissmatrix);

        disableHierarchical = false;
      }

      /* Compute the reverse Cuthill-McKee order. */
      const revCuthillLookup = reordering.cuthillMcKee(nodes).reverse();

      /* Compute the Moment Ordering. */
      let momentRowLookup, momentColLookup;
      [momentRowLookup, momentColLookup] = reordering.momentOrder(nodes);

      /* Sort the nodes by in/outdegree. */
      let degRowLookup, degColLookup;
      [degRowLookup, degColLookup] = reordering.sortedByDegree(nodes);

      let pageRankLookup;
      if (!disablePagerank) {
        pageRankLookup = reordering.pageRank(matrix);
      }

      const revCuthillOrder = new Array(nodes.length);
      const momentRowOrder = new Array(nodes.length);
      const momentColOrder = new Array(nodes.length);
      const degRowOrder = new Array(nodes.length);
      const degColOrder = new Array(nodes.length);
      const pageRankOrder = new Array(nodes.length);

      let slinkOrder, clinkOrder;
      if (!disableHierarchical) {
        slinkOrder = new Array(nodes.length);
        clinkOrder = new Array(nodes.length);
      }

      /* Properly order the arrays. */
      for (let i = 0; i < nodes.length; i++) {

        if (!disableHierarchical) {
          slinkOrder[slinkLookup[i]] = i;
          clinkOrder[clinkLookup[i]] = i;
        }

        revCuthillOrder[revCuthillLookup[i]] = i;

        momentRowOrder[momentRowLookup[i]] = i;
        momentColOrder[momentColLookup[i]] = i;

        degRowOrder[degRowLookup[i]] = i;
        degColOrder[degColLookup[i]] = i;

        if (!disablePagerank) {
          pageRankOrder[pageRankLookup[i]] = i;
        }
      }


      let hierarchy = undefined;

      if (!disableHierarchical) {

        hierarchy = {
          slink: {
            order: slinkOrder,
            lookup: slinkLookup,
            parents: slinkParents,
            heights: slinkHeights,
            min_height: slinkMinHeight,
          },

          clink: {
            order: clinkOrder,
            lookup: clinkLookup,
            parents: clinkParents,
            heights: clinkHeights,
            min_height: clinkMinHeight,
          },
        };
      };

    const orderIndices = {
      revCuthill: revCuthillOrder,
      momentRow: momentRowOrder,
      momentCol: momentColOrder,
      degRow: degRowOrder,
      degCol: degColOrder,
    };

    const lookupIndices = {
      revCuthill: revCuthillLookup,
      momentRow: momentRowLookup,
      momentCol: momentColLookup,
      degRow: degRowLookup,
      degCol: degColLookup,
    };

    if (!disablePagerank) {
      orderIndices.pageRank = pageRankOrder;
      lookupIndices.pageRank = pageRankLookup;
    }

    saveData(filename, JSON.stringify({
      symmetric: symmetric,
      biggest: biggest,
      mean: mean,
      maxout: maxout,
      data: nodes,
      orderIndices: orderIndices,
      lookupIndices: lookupIndices,
      hierarchy: hierarchy,
    }), callback)
  });
}

function saveData(filename, data, callback) {
  // ------------------------ SAVING THE FILE -----------------------------
  const directory = path.resolve('uploads');
  //remove extension
  filename = filename.split('.').slice(0, -1).join('.');

  fs.readdir(directory, (errReadDir, file_names) => {
    if (errReadDir) {
      return callback(errReadDir);
    }

    if (!Array.isArray(file_names)) {
      return callback('Error when reading _datasets directory: file_names is not an array');
    }

    // if file already exists, append a '-' to the end
    while (file_names.find(file => filename + '.json' === file) !== undefined) {
      filename += ' - Copy';
    }

    fs.writeFile(directory + '/' + filename + '.json', data, (errWrite) => {
      if (errWrite) {
        return callback(errWrite);
      }

      // succesful, so no error
      if (typeof callback === 'function') {
        callback();
      }
    });

    fileLoc = directory + path.sep + filename + '.json';
    pythonLoc = 'cluster.py';
    clusterAmount = 5;

    dataObj = JSON.parse(data);
    if (dataObj.data.length < 1500) {

      exec('cd ..', (err) => {
        if (err) {
          console.log(err);
          return;
        }
      })

      if (process.platform === 'linux') {
        pyVer = 'python3.6'
      } else {
        pyVer = 'python'
      }

      exec(pyVer + ' ' + pythonLoc + ' "' + fileLoc + '" ' + clusterAmount, (err) => {
        if (err) {
          console.log(err);
          return;
        }
      })

    }

  });

}

module.exports = {
  parse: parse
};
