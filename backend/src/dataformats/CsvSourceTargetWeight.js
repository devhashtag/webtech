const Papa = require('papaparse');

/**
 * This function parses the given csv string and creates a 2D array out of it
 * Format (delimiter is detected automatically):
 * SOURCE, TARGET, WEIGHT(optional)
 * More attributes will be ignored
 * @param data
 */
function parseData(data) {
  const PapaData = Papa.parse(data);

  // Check data format
  const headerRow = PapaData.data.shift();
  if (headerRow[0] !== 'SOURCE' || headerRow[1] !== 'TARGET') {
    throw 'Parsing failed, invalid format';
  }

  // Detect weighted graph
  const weighted = headerRow[2] === 'WEIGHT';

  /**
   * JSON structure:
   * [
   * [source, target, weight (optional)]
   * ]
   */
  const parsedData = [];

  // max value for scaling
  let max = 0;
  if (weighted) {
    for (const row of PapaData.data) {
      if (row[2] > max) {
        max = row[2];
      }
    }
  }

  for (const row of PapaData.data) {
    if (weighted) {
      let weight_scaled = row[2] / max;
      parsedData.push([row[0], row[1], weight_scaled])
    } else {
      parsedData.push([row[0], row[1]])
    }
  }
  return parsedData;
}

module.exports = {
  parseData: parseData
};