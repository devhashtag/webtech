const express = require('express');
const formidable = require('formidable');
const router = express.Router();
const fs = require('fs');
const path = require('path');

const parser = require('../parser');

router.get('/', function(req, res, next) {
   fs.readdir(path.resolve('uploads'), (readErr, file_names) => {
        if (readErr) {
            console.log(`Error while reading datasets: ${readErr}`);
            return res.status(500).json({ message: 'Cannot read datasets' });
        }

        res.json(file_names);
   });
});

router.get('/ping', function (req, res, next) {
    res.send(new Date());
});

router.get('/:filename', function(req, res, next) {
    const filename = req.params.filename;

    fs.readFile(path.resolve('uploads') + '/' + path.basename(filename), (readErr, buffer) => {
        if (readErr) {
            console.log(`Error while reading dataset: ${readErr}`);
            return res.status(500).json({ message: 'Cannot read dataset' });
        }
        let data = JSON.parse(buffer.toString());

        // add filename for use in client
        data.name = path.basename(filename);

        res.json({
            dataset: data
        });
    });
});

router.post('/upload', function(req, res, next) {
    new formidable.IncomingForm().parse(req, function(err, fields, files) {
        if (err) {
            console.error(`There was an error while parsing uploaded dataset: ${err}`);
            return res.status(500).json({ message: 'Cannot parse file' });
        }

        const uploadInput = files.uploadInput;

        if (!uploadInput || !uploadInput.path) {
            return res.status(400).json({ message: 'no file uploaded' });
        }

        parser.parse(uploadInput.path, uploadInput.name, (errParse) => {
            if (errParse) {
                console.error(errParse);
                return res.status(500).json({message: 'Error while parsing'});
            }

            return res.json({ message: 'succesfully parsed dataset'});
        });
    });
});

// router.get('/test', function (req, res, next) {
//     try {
//         const filepath = path.resolve('../_datasets/GephiMatrix_co-citation.csv');
//         parser.parse(filepath, 'GephiMatrix_co-citation.json', (errParse) => {
//             if (errParse) {
//                 console.error(errParse);
//                 return res.status(500).json({message: 'Error while parsing'});
//             }
//
//             return res.json({ message: 'succesfully parsed dataset'});
//         });
//     } catch (e) {
//         res.send('Invalid request');
//     }
// });


module.exports = router;