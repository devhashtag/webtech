const express = require('express');
const formidable = require('formidable');
const router = express.Router();
const fs = require('fs');
const path = require('path');

router.get('/', function(req, res, next) {
    // if the directory does not exists, we create it
    if (!fs.existsSync(path.resolve('stored_canvas'))) {
        fs.mkdirSync(path.resolve('stored_canvas'));
    }

    fs.readdir(path.resolve('stored_canvas'), (readError, files) => {
        if (readError) {
            console.log('Error while reading stored_canvas directory: ', readError);
            return res.status(500).json({
                message: 'There was an error'
            });
        }

        res.json({
            files: files
        });
    });
});

router.get('/:filename', function(req, res, next) {
    const full_path = path.resolve('stored_canvas') + '\\' + req.params.filename;

    if (!fs.existsSync(full_path)) {
        return res.status(400).json({
            message: 'This file does not exist'
        });
    }

    fs.readFile(full_path, (readError, contents) => {
        if (readError) {
            console.error(`Error while reading file: '${full_path}' : `, readError);
            return res.status(500).json({
                message: 'There was an error'
            });
        }

        try {
            const obj = JSON.parse(contents.toString());

            res.json(obj);
        } catch(ex) {
            console.error(`There was an error whilst parsing a canvas object: ${ex.toString()}`);
            res.status(500).json({
                message: 'There was an error whilst parsing the file'
            });
        }
    });
});

router.post('/upload/:filename', function(req, res, next) {
    const content = req.body.content;
    const filename = req.params.filename;
    const full_path = path.resolve('stored_canvas') + '\\' + filename;

    if (fs.existsSync(full_path)) {
        return res.status(400).json({
            message: 'File already exists!'
        });
    }

    fs.writeFile(full_path, JSON.stringify(content), writeError => {
        if (writeError) {
            console.error(`There was an error whilst writing to file: '${full_path}' : `, writeError);
            return res.status(500).json({
                message: 'There was an error'
            });
        }

        res.json({
            message: 'File created'
        });
    });
});

router.delete('/:filename', function(req, res, next) {
    const full_path = path.resolve('stored_canvas') + '\\' + req.params.filename;

    if (!fs.exists(full_path)) {
        return res.status(400).json({
            message: 'The specified file does not exist'
        });
    }

    fs.unlink(full_path, deleteError => {
        if (deleteError) {
            console.error(`Error while removing file: '${full_path}' : `, deleteError);
            return res.status(500).json({
                message: 'There was an error'
            });
        }

        res.json({
            message: 'File deleted succesfully'
        });
    });
});

module.exports = router;