const { GPU } = require('gpu.js'); 

/**
 * @author M.H.M. Leus (1315366)
 * @param permutation The permatation that needs to be indexable.
 */
function indexablePermutation(permutation) {

    const idxPermutation = new Array(permutation.length);

    for (let i = 0; i < permutation.length; ++i) {
        idxPermutation[permutation[i]] = i;
    }

    return idxPermutation;
}

/**
 * Internal function for gpu euclidian distance.
 * @author M.H.M. Leus (1315366)
*/
function _gpu_dist_euclidian(a) {
    let dist = 0;
    let interdist = 0;

    for (let i = 0; i < this.constants.size; ++i) {
        interdist = a[i][this.thread.y] - a[i][this.thread.x];
        dist += interdist * interdist
    }

    return Math.sqrt(dist);
}

/**
 * Internal function for cpu euclidian distance.
 * @author M.H.M. Leus (1315366)
*/
function _cpu_dist_euclidian(a, i, j) { 
    let dist = 0;
    let interdist = 0;

    for (let k = 0; k < a.length; ++k) {
        interdist = a[k][i] - a[k][j];
        dist += interdist * interdist;
    }

    return Math.sqrt(dist);
}

/**
 * Internal function for gpu squared euclidian distance.
 * @author M.H.M. Leus (1315366)
*/
function _gpu_dist_euclidian_square(a) {
    let dist = 0;
    let interdist = 0;

    for (let i = 0; i < this.constants.size; ++i) {
        interdist = a[i][this.thread.y] - a[i][this.thread.x];
        dist += interdist * interdist
    }

    return dist;
}

/**
 * Internal function for cpu squared euclidian distance.
 * @author M.H.M. Leus (1315366)
*/
function _cpu_dist_euclidian_square(a, i, j) { 
    let dist = 0;
    let interdist = 0;

    for (let k = 0; k < a.length; ++k) {
        interdist = a[k][i] - a[k][j];
        dist += interdist * interdist;
    }

    return dist;
}

/**
 * Calculate a dissimilarity matrix on the GPU.
 * @author M.H.M. Leus (1315366)
 * @param matrix The matrix for which the dissimilarity matrix calculated.
*/
function _gpu_dissimilarityMatrix(matrix, metric = 0) {

    const gpu = new GPU({mode: 'headlessgl'});

    /* Create settings for the gpu algorithm */
    kernelSettings = {
        output: [matrix.length, matrix.length],
        constants: { size: matrix.length }, 
    };

    let kFunc;
    switch(metric) {
        case 0:
            kFunc = _gpu_dist_euclidian;
            break;
        case 1:
            kFunc = _gpu_dist_euclidian_square;
            break;
        default:
            kFunc = _gpu_dist_euclidian;
    };

    const kernel = gpu.createKernel(kFunc, kernelSettings);
    
    return kernel(matrix);
}

/**
 * Calculate the lower triangular (and the diagonal) part of a dissimilarity matrix on the CPU.
 * @author M.H.M. Leus (1315366)
 * @param matrix The matrix for which the dissimilarity matrix calculated.
*/
function _cpu_dissimilarityMatrix(matrix, metric = 0) {

    const diss = new Array(matrix.length);

    let dist;
    switch(metric) {
        case 0:
            dist = _cpu_dist_euclidian;
            break;
        case 1:
            dist = _cpu_dist_euclidian_square;
            break;
        default:
            dist = _cpu_dist_euclidian;
    };

    for (let i = 0; i < matrix.length; ++i) {

        const arr = new Float32Array(i + 1);

        /* Calculate all potentially nonzero distances. */
        for (let j = 0; j < i; ++j) {
            arr[j] = dist(matrix, i, j);
        }

        /* Distance from i to itself is always 0. */
        arr[i] = 0;

        diss[i] = arr;
    }

    return diss;
}

function dissimilarityMatrix(matrix, metric = 0) {

    if (GPU.isHeadlessGLSupported) {
        return _gpu_dissimilarityMatrix(matrix, metric);
    } else {
        return _cpu_dissimilarityMatrix(matrix, metric);
    }

}

/**
 * The (reverse) Cuthill-McKee algorithm.
 * @author M.H.M. Leus (1315366)
*/
function cuthillMcKee(nodes) {

    const length = nodes.length;
    const R = [];

    /* Array to keep track of which nodes have already been visited. */
    const visited = new Array(length).fill(false);

    /* Keep track of an index for which all previous nodes have already been visited. */
    let lastCertainlyVisited = 0;

    /* Find all nodes of degree zero and add them to the start of the list. */
    for (let i = 0; i < nodes.length; i++) {

        if (nodes[i].indegree + nodes[i].outdegree === 0) {
            R.push(i);
            visited[i] = true;
        }
        else if (lastCertainlyVisited === 0) {
            lastCertainlyVisited = i;
        }

    }

    /* The queue starts after the zero-degree nodes. */
    let queueStart = R.length;

    while (true) {

        let minDegNode = lastCertainlyVisited;

        /* Find the first unvisited node. */
        while (visited[minDegNode]) {
            minDegNode += 1;
        }

        lastCertainlyVisited = minDegNode;

        minDegree = nodes[minDegNode].indegree + nodes[minDegNode].outdegree;

        /*
            Find the visited node with the least degree.
            The loop starts at minDegNode + 1, as every earlier node has already been visited.
        */
        for (let i = minDegNode + 1; i < length; ++i) {

            /* Don't consider that have already been visited. */
            if (visited[i]) {
                continue;
            }

            let currentDegree = nodes[i].indegree + nodes[i].outdegree

            if (currentDegree < minDegree) {
                minDegNode = i;
                minDegree = currentDegree;
            }
        }

        R.push(minDegNode);
        visited[minDegNode] = true;
        
        while (queueStart < R.length) {
            const v = R[queueStart];
            queueStart += 1;

            const A = [];

            /* Add all unvisited outgoing neighbours of v to A. */
            for (let endpoint in nodes[v].outgoingEdges) {
                let u = parseInt(endpoint);

                if (!visited[u] && nodes[v].outgoingEdges[u] !== 0) {
                    A.push(u);
                    visited[u] = true;
                }
            }

            /* Add all unvisited incoming neighbours of v to A. */
            for (let endpoint in nodes[v].incomingEdges) {
                let u = parseInt(endpoint);

                if (!visited[u] && nodes[v].incomingEdges[u] !== 0) {
                    A.push(u);
                    visited[u] = true;
                }
            }

            /* Sort the nodes in A in nondecreasing order by degree. */
            A.sort((a, b) => (nodes[a].indegree + nodes[a].outdegree) - (nodes[b].indegree + nodes[b].outdegree));

            /* 
                Append the items in A to R. 
                Code taken from:
                https://dev.to/uilicious/javascript-array-push-is-945x-faster-than-array-concat-1oki
            */
            const originalLength = R.length;        
            R.length = originalLength + A.length;

            for (let i = 0; i < A.length; ++i) {
                R[originalLength + i] = A[i];
            }

        }
        
        /* When every node has been added, return the permutation array. */
        if (R.length >= length) {
            return R;
        }
    }

}

/**
 * The moment order algorithm.
 * @author M.H.M. Leus (1315366)
*/
function momentOrder(nodes) {

    let rowmoments = new Array(nodes.length);
    let colmoments = new Array(nodes.length);

    let roworder = new Array(nodes.length);
    let colorder = new Array(nodes.length);

    let rowindex = new Array(nodes.length);
    let colindex = new Array(nodes.length);

    for (let i = 0; i < nodes.length; ++i) {

        /* Create the identitity permutation */
        roworder[i] = colorder[i] = i;

        /* calculate initial row moments */
        rowmoments[i] = 0;

        let outgoing = nodes[i].outgoingEdges;

        for (let key in outgoing) {
            rowmoments[i] += (parseInt(key) + 1) * outgoing[key];
        }
        rowmoments[i] /= nodes[i].outdegree;        
    }

    /* Sort rows by nondecreasing row moment. */
    roworder.sort((a, b) => rowmoments[a] - rowmoments[b]);

    /* Restructure the roworder array to allow for quick quick index lookup. */
    for (let i = 0; i < nodes.length; ++i) {
        rowindex[roworder[i]] = i;
    }

    let iter = 0;

    /* Limit iterations to ensure termination. */
    while (iter < nodes.length) {
    
        /* Calculate column moments */
        let sorted = true;
        let previousColMoment = 0;

        for (let i = 0; i < nodes.length; ++i) {
            
            let moment = 0;

            let incoming = nodes[i].incomingEdges;

            /* Loop over all incoming edges */
            for (let key in incoming) {
                colmoments[i] += (rowindex[parseInt(key)] + 1) * incoming[key];
            }
            moment /= nodes[i].indegree;

            if (moment < previousColMoment) { sorted = false; }

            previousColMoment = colmoments[i] = moment;
            
        }
        
        /* Rows are already sorted, only need to check columns */
        if (sorted) {
            return [roworder, colorder];
        }

        colorder.sort((a, b) => colmoments[a] - colmoments[b]);

        /* Restructure the colorder array to allow for quick index lookup. */
        for (let i = 0; i < nodes.length; ++i) {
            colindex[colorder[i]] = i;
        }

        /* Calculate row moments. */
        sorted = true;
        let previousRowMoment = 0;

        for (let i = 1; i < nodes.length; ++i) {

            let outgoing = nodes[i].outgoingEdges;

            /* Loop over all outgoing edges */
            for (let key in outgoing) {
                rowmoments[i] += (colindex[parseInt(key)] + 1) * outgoing[key];
            }
            moment /= nodes[i].outdegree;

            if (moment < previousRowMoment) { sorted = false; }

            previousRowMoment = rowmoments[i] = moment;

        }

        /* Columns are already sorted, only need to check rows */
        if (sorted) {
            return [roworder, colorder];
        }

        roworder.sort((a, b) => rowmoments[a] - rowmoments[b]);

        /* Restructure the roworder array to allow for quick index lookup. */
        for (let i = 0; i < nodes.length; ++i) {
            rowindex[roworder[i]] = i;
        }
        
        ++iter;
    }

    console.log("Limiting at " + iter + "iterations.");
    return [roworder, colorder];

}

/**
 * Sort the rows and columns in nondecreasing order
 * @author M.H.M. Leus (1315366)
*/
function sortedByDegree(nodes) {

    let roworder = new Array(nodes.length);
    let colorder = new Array(nodes.length);

    for (let i = 0; i < nodes.length; ++i) {

        /* Create identity permutation */
        roworder[i] = colorder[i] = i;
    }

    roworder.sort((a, b) => nodes[a].outdegree - nodes[b].outdegree);
    colorder.sort((a, b) => nodes[a].indegree - nodes[b].indegree);

    return [roworder, colorder];
}


/**
 * The SLINK algorithm for single-linkage hierarchical clustering.
 * @author M.H.M. Leus (1315366)
 * @param dissmat The matrix of dissimilarity coefficients
*/
function slink(dissmat) {
    
    const pies = new Array(dissmat.length);
    const lambdas = new Array(dissmat.length);
    const mus = new Array(dissmat.length);

    pies[0] = 0;
    lambdas[0] = Infinity;

    /* t = n+1 */
    for (let t = 1; t < dissmat.length; ++t) {

        /* Step 1 */
        pies[t] = t;
        lambdas[t] = Infinity;

        /* Step 2 */
        for (let i = 0; i < t; ++i) {
            /* i < t, so this is safe for the lower triangular CPU matrix. */
            mus[i] = dissmat[t][i];
        }

        /* Step 3 */
        for (let i = 0; i < t; ++i) {

            if (lambdas[i] >= mus[i]) {
                mus[pies[i]] = Math.min(mus[pies[i]], lambdas[i]);
                lambdas[i] = mus[i];
                pies[i] = t;
            } 
            else {
                mus[pies[i]] = Math.min(mus[pies[i]], mus[i]);
            }
        }

        /* Step 4 */
        for (let i = 0; i < t; ++i) {
            if (lambdas[i] >= lambdas[pies[i]]) {
                pies[i] = t;
            }
        }
    }
    const pointer_min = pack_pointer(pies, lambdas);

    return [pointer_min[0], pies, lambdas, pointer_min[1]];
}


/**
 * The CLINK algorithm for complete-linkage hierarchical clustering.
 * @author M.H.M. Leus (1315366)
 * @param dissmat The matrix of dissimilarity coefficients
*/
function clink(dissmat) {

    const pies = new Array(dissmat.length);
    const lambdas = new Array(dissmat.length);
    const mus = new Array(dissmat.length);

    pies[0] = 0;
    lambdas[0] = Infinity;

    /* t = n+1 */
    for (let t = 1; t < dissmat.length; ++t) {

        /* Step 1 */
        pies[t] = t;
        lambdas[t] = Infinity;

        /* Step 2 */
        for (let i = 0; i < t; ++i) {
            /* i < t, so this is safe for the lower triangular CPU matrix. */
            mus[i] = dissmat[t][i];
        }

        /* Step 3 */
        for (let i = 0; i < t; ++i) {
            if (lambdas[i] < mus[i]) {
                mus[pies[i]] = Math.max(mus[pies[i]], mus[i]);
                mus[i] = Infinity;
            }
        }

        /* Step 4 */
        let a = t - 1;

        /* Step 5 */
        for (let i = 0; i < t; ++i) {

            if (lambdas[t-i] >= mus[pies[t-i]]) {
                if (mus[t-i] < mus[a]) {
                    a = t - i;
                }
            }
            else {
                mus[t-i] = Infinity;
            }
        }

        /* Step 6 */
        let b = pies[a];
        let c = lambdas[a];
        pies[a] = t;
        lambdas[a] = mus[a];

        /* Step 7 */
        if (a < t-1) {

            while (b < t-1) {
                let d = pies[b];
                let e = lambdas[b];
                pies[b] = t;
                lambdas[b] = c;
                b = d;
                c = e;
            }

            if (b === t-1) {
                pies[b] = t;
                lambdas[b] = c;
            }
        }

        /* Step 8 */
        for (let i = 0; i < t; ++i) {
            if ( (pies[pies[i]] === t) && (lambdas[i] >= lambdas[pies[i]]) ) {
                pies[i] = t;
            }
        }
    }

    const pointer_min = pack_pointer(pies, lambdas);

    return [pointer_min[0], pies, lambdas, pointer_min[1]];
}


/**
 * Convert a pointer representation into a packed representation.
 * @author M.H.M. Leus (1315366)
 * @param pies The pis of the pointer representation
 * @param lambdas The lambdas of the pointer representation
*/
function pack_pointer(pies, lambdas) {

    const output = [];
    const children = new Array(pies.length);

    for (let i = 0; i < pies.length; ++i) {
        children[i] = [];
    }

    /* Add nodes to their parent's children array. */
    for (let i = 0; i < pies.length - 1; ++i) {
        children[pies[i]].push(i);
    }

    let min_lambda = Infinity;

    /* Sort children by nonincreasing lambda. */
    for (let i = 0; i < pies.length; i++) {
        children[i].sort((a, b) => lambdas[b] - lambdas[a]);

        /* Simultaneously find the minimum lambda. */
        const smallest = children[i][children[i].length - 1]
        if (smallest < min_lambda) {
            min_lambda = smallest;
        }
    }

    /* Bottom-up stack. */
    const stack = new Array(pies.length);
    stack[0] = pies.length - 1;

    let sp = 0;

    /* Keep track of how many children of a node's children have already been pushed to the stack. */
    const pushed = new Array(pies.length).fill(0);

    /* Repeat while the stack is not empty. */
    while (sp >= 0) {

        const parent = stack[sp];

        /* Test if there unvisited children left. */
        if (pushed[parent] < children[parent].length) {

            /* Push the next child to the stack. */
            stack[++sp] = children[parent][pushed[parent]++];
        } 
        else {
            /* Have sp point to parent's parent. */
            --sp;

            /* Add parent to the output order. */
            output.push(parent);
        }

    }

    return [output, min_lambda];
}

const math = require('../node_modules/mathjs/dist/math.js');

function pageRank(matrix) {

    let matrixSize = matrix.length;

    // Making matrix column stochastic
    for (let column = 0; column < matrixSize; column++) {

        let columnSum = 0;
        // First calculate column sum of the matrix
        for (let row = 0; row < matrixSize; row++) {
            columnSum += matrix[row][column];
        }

        // Make matrix column stochastic
        if (columnSum == 0) {
            // If whole column is 0 give every page same importance
            for (let row = 0; row < matrixSize; row++) {
                matrix[row][column] = 1 / matrixSize;
            }
        } else {
            // Them divide column by this sum
            for (let row = 0; row < matrixSize; row++) {
                matrix[row][column] /= columnSum;
            }
        }
    }


    // Making matrix pageRank ready
    for (let column = 0; column < matrixSize; column++) {
        for (let row = 0; row < matrixSize; row++) {
            matrix[row][column] = 0.85 * matrix[row][column] + 0.15 * (1 / matrixSize);

            // if (row == column) {
            //     matrix[row][column] -= 1;
            // }

            matrix[row][column] = Math.round(matrix[row][column] * 10000) / 10000;
        }
    }


    // Making xvector, corresponding to starting in website A
    let xvector = new Array(matrixSize);
    xvector.fill(0, 1);
    xvector[0] = 1;

    // Save original matrix
    let pageRank = math.multiply(matrix, xvector)

    // Calculate pagerank for 64 iterations (is enough when comparing to answers)
    for (let iteration = 0; iteration < 32; iteration++) {
        pageRank = math.multiply(matrix, pageRank)
    }

    let pageRankOrder = new Array(matrixSize);

    for (let index = 0; index < matrixSize; index++) {
        pageRankOrder[index] = index
    }

    pageRankOrder.sort((a, b) => pageRank[a] < pageRank[b]);

    return pageRankOrder;
}


module.exports = {
    dissimilarityMatrix: dissimilarityMatrix,

    cuthillMcKee: cuthillMcKee,
    momentOrder: momentOrder,
    sortedByDegree: sortedByDegree,
    indexablePermutation: indexablePermutation,
    slink: slink,
    clink: clink,
    pageRank: pageRank
}