import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.css']
})
export class BackgroundComponent implements OnInit {

  ngOnInit() {
    const c = <HTMLCanvasElement>document.getElementById('c');
    const context = c.getContext('2d');
    const font_size = 20;
    let columns;
    let rain;

    // making the canvas full screen
    function setSize() {
      c.height = document.documentElement.clientHeight;
      c.width = document.documentElement.clientWidth;
      columns = c.width / font_size;
      rain = [];
      for (let x = 0; x < columns; x++) {
        rain[x] = c.height;
      }
    }

    setSize();
    window.addEventListener('resize', setSize);
    // chinese characters - taken from the unicode charset
    // "٦ȳێǴǩĬ˒۔ƊĘڸʆެݟۍȎćЪЈξʛ̤¨òܒۺӡɏòɖڝ¯ݼޙ٤̒χןǎЄ֖ΕâɯŞرȌРʛ͸ш΢тˤٴnȅϣـ͍ǂɵ΄ۙѩ̈́ݧץȶئ̹ƓѰŜޫҚѭŦĆ޷ӲέĿĶȎáؿئnʈ̱٫ƶږ٢ӲŷʙãϔؙĀؒӱϒŽ̧ǻʮ܂ց֚ŲæԞʍк¿Ňǎ";
    const chars = '田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑';
    // converting the string into an array of single characters
    const charArray = chars.split('');

    // drawing the characters
    function draw() {
      // Black BG for the canvas
      // translucent BG to show trail
      context.fillStyle = 'rgba(255, 255, 255, 0.08)';
      context.fillRect(0, 0, c.width, c.height);

      context.fillStyle = 'rgba(173,173,173,0.4)';
      context.font = font_size + 'px arial';
      // looping over rain
      for (let i = 0; i < rain.length; i++) {
        // a random chinese character to print
        const text = charArray[Math.floor(Math.random() * charArray.length)];
        // x = i*font_size, y = value of rain[i]*font_size
        context.fillText(text, i * font_size, rain[i] * font_size);

        // sending the drop back to the top randomly after it has crossed the screen
        // adding a randomness to the reset to make the rain scattered on the Y axis
        if (rain[i] * font_size > c.height && Math.random() > 0.975) {
          rain[i] = 0;
        }

        // incrementing Y coordinate
        rain[i]++;
      }
    }

    setInterval(draw, 50);
  }
}
