// -------------------------------- import ----------------------------
import { Component, OnInit, ViewChild, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import { DatasetService } from '../services/dataset.service';
import * as PIXI from 'pixi.js';
import * as Viewport from 'pixi-viewport';
import { Subject } from 'rxjs';
import { Edge } from '../interfaces/edge.interface';
import { enableDebugTools } from '@angular/platform-browser';

// -------------------------------- configurations ----------------------------
@Component({
  selector: 'app-heatmap',
  templateUrl: './heatmap.component.html',
  styleUrls: ['./heatmap.component.css'],
})

// -------------------------------- variables / constant declerations ----------------------------
// onderstaande regel maakt een class net als in java, de export ervoor zegt eigenlijk dat dit de hoofdclass van het bestand is
export class HeatmapComponent implements OnInit {

  // dit zorgt ervoor dat er in het variabele pixiContainer de div #pixiContainer komt (van het html template)
  @ViewChild('pixiContainer') pixiContainer;

  public pApp: any;

  // other pixi variables/objects
  private viewport: Viewport;
  private container: PIXI.Container;
  private graphics: PIXI.Graphics;

  public selectedData = null;
  public nodeIndexes = {};
  public rowIndexes = {};
  public colIndexes = {};
  public _selectedLayout = 'cuthill';

  @Input() set selectedLayout(layout: string) {
    this._selectedLayout = layout.toLowerCase();
    if (this._dataset) {
      this.updateLayout();
    }
  }

  public _dataset;
  public _selectedInput: Edge[];

  private selectionMode = false;
  private selectedArea: any;
  private selectedSprite: PIXI.Sprite;

  public twenty;
  public fourty;
  public sixty;
  public eighty;
  public hundred;

  public grayscale;

  public tinyColorArray = [19, 155, 212];
  public smallColorArray = [140, 198, 91];
  public mediumColorArray = [237, 218, 104];
  public largeColorArray = [243, 147, 68];
  public hugeColorArray = [254, 1, 0];
  public backGroundColor = '#FFFFFF';

  private appSizeX = 0;
  private appSizeY = 0;
  public _colorArray = [];
  public _colorArraySel = [255, 0, 0];

  @Input() set colorArray(value: any) {
    this._colorArray = value;
    if (this.graphics) {
      this.graphics.removeChildren();
      this.drawMatrix(Math.min(this.appSizeX, this._dataset.data.length), Math.min(this.appSizeY, this._dataset.data.length));
    }
  }

  public widthSprite;
  public heightSprite;
  public sprite;
  public mapSize;

  @Output() selectedEvent = new EventEmitter<any>();

  onDatasetChange = new Subject();

  @Input() set dataset(value: any) {
    this._dataset = value;

    // calculate the indexes of the nodes
    for (const i in this._dataset.data) {
      if (this._dataset.data.hasOwnProperty(i)) {
        const obj = this._dataset.data[i];
        this.nodeIndexes[obj.nodeId] = parseInt(i, 10);
      }
    }

    this.rowIndexes = this.colIndexes = this.nodeIndexes;
    this.onDatasetChange.next();
  }

  onSelectedInputChange = new Subject();

  @Input() set selectedInput(value: Edge[]) {
    this._selectedInput = value;
    this.onSelectedInputChange.next();
  }


  constructor(private datasetService: DatasetService) { }

  resize(heatmap: boolean, graph: boolean, swapped: boolean) {
    const root = document.documentElement;

    if (heatmap && graph) {
      root.style.setProperty('--leftPosition', (swapped) ? "0vw" : "50vw");
      root.style.setProperty('--widthLength', "50vw");
      this.pApp.renderer.view.style.width = `${this.mapSize}px`;
    } else {
      root.style.setProperty('--leftPosition', "0");
      root.style.setProperty('--widthLength', "100vw");
      this.pApp.renderer.view.style.width = `${this.mapSize * 2}px`;
    }


  }

// -------------------------------- initialization ----------------------------
  ngOnInit() {
    // constant self voor in verschillende functies
    const self = this;

    // de console message mag opflikkeren
    PIXI.utils.skipHello();

    this.twenty = this._dataset.biggest * 0.2;
    this.fourty = this._dataset.biggest * 0.4;
    this.sixty = this._dataset.biggest * 0.6;
    this.eighty = this._dataset.biggest * 0.8;
    this.hundred = this._dataset.biggest;
    this.grayscale = true;

    // Renderen wordt op nearest gezet om wazigheid te voorkomen
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

    // Hier kan je de viewsize en resolutie van de heatmap aanpassen
    this.mapSize = window.screen.width / 2;

    // bepaal grootte van size van applicatie
    this.appSizeX = this._dataset.data.length;
    this.appSizeY = this._dataset.data.length;

    // minimum size to avoid shitty behaviour
    this.appSizeX = this.appSizeX < 500 ? 500 : this.appSizeX;
    this.appSizeY = this.appSizeY < 500 ? 500 : this.appSizeY;

// -------------------------------- creating pixi (viewport) application ----------------------------
    this.pApp = new PIXI.Application({
      width: this.appSizeX,              // breedte gelijk aan grootte dataset
      height: this.appSizeY,             // idem dito above
      transparent: true,            // transparante achtergrond
      antialias: false,              // dit zorgt voor scherpere lijnen enzo
      forceCanvas: true
    });

    // Hier wordt PIXI app op de webapp geplaatst
    this.pixiContainer.nativeElement.appendChild(this.pApp.view);

    // Hier wordt een viewport object gemaakt voor pan and zoom functies
    this.viewport = new Viewport({
      screenWidth: this.appSizeX,        // schermgrootte = grootte applicatie
      screenHeight: this.appSizeY,       // idem dito
      worldWidth: this.appSizeX,         // 'wereldgrootte', vooral belangrijk voor borders
      worldHeight: this.appSizeY,
      passiveWheel: false,          // onder andere om scrollevent te cancelen

      // the interaction module is important for wheel() to work properly when renderer.view is placed or scaled
      interaction: this.pApp.renderer.plugins.interaction
    });

// -------------------------------- setting functions for viewport ----------------------------
    this.viewport
      // .on('clicked', click)
      .on('rightdown', mousedown)
      .on('rightup', mouseup)
      .on('mousemove', mousemove)
      .drag()         // voor slepen
      .pinch()        // voor zoomen met vingers
      .wheel()        // voor scrollen
      .decelerate()   // voor nabeweging
      // .bounce({
      //  time: 1000,                // voor borders van Graph
      //  sides: 'all',
      //  underflow: 'center'
      // })
      // .clampZoom({
      //   minHeight: 0.05 * this.appSizeY,   // voor max inzoomen en uitzoomen
      //   maxHeight: 1.1 * this.appSizeY,
      //   minWidth: 0.05 * this.appSizeX,
      //   maxWidth: 1.1 * this.appSizeX
      // });

    // Viewport wordt toegevoegd aan stage
    this.pApp.stage.addChild(this.viewport);


// -------------------------------- declaring important PIXI components  ----------------------------
    // Container en graphics worden aangemaakt
    this.container = new PIXI.Container();
    this.graphics = new PIXI.Graphics();

    // Deze worden toegevoegd aan pan and zoom object
    this.viewport.addChild(this.container);
    this.viewport.addChild(this.graphics);

    // Move container to the center
    this.container.x = this.pApp.screen.width / 2;
    this.container.y = this.pApp.screen.height / 2;

    // Sprite die select blok laat zien
    this.sprite = (new PIXI.Sprite(PIXI.Texture.WHITE));
    this.widthSprite = (new PIXI.Sprite(PIXI.Texture.WHITE));
    this.heightSprite = (new PIXI.Sprite(PIXI.Texture.WHITE));

    this.heightSprite.tint = this.widthSprite.tint = 0xFF0000;
    this.heightSprite.alpha = this.widthSprite.alpha = 0.5;

    this.sprite.tint = 0xFF0000;
    this.sprite.alpha = 0.5;
    this.sprite.width = this.sprite.height = 1;

    // Hier wordt de size van de heatmap geset
    this.pApp.renderer.view.style.width = `${this.mapSize}px`;

    // initialize
    this.updateLayout();
    this.drawMatrix(Math.min(this.appSizeX, this._dataset.data.length), Math.min(this.appSizeY, this._dataset.data.length));

// -------------------------------- interaction with heatmap ----------------------------
    function mousedown(event) {
      // set selection mode to true
      self.selectionMode = true;

      // disable panning
      self.viewport.drag({factor: 0});

      const x = event.data.global.x;
      const y = event.data.global.y;

      // offset (already scaled)
      const top = self.viewport.top;
      const left = self.viewport.left;

      const scale = self.viewport.scale;

      // scale coordinates (i.e. these are the 'pixel'-coordinates)
      const worldX = Math.floor(x / scale.x + left);
      const worldY = Math.floor(y / scale.y + top);

      // make a 1 by 1 selection, in case the button gets released with moving the mouse
      self.selectedArea = {x1: worldX, y1: worldY, x2: worldX + 1, y2: worldY + 1};

      // draw the selection
      self.drawSelected();
    }

    function mousemove(event) {
      if (self.selectionMode) {
        const x = event.data.global.x;
        const y = event.data.global.y;

        // offset (already scaled)
        const top = self.viewport.top;
        const left = self.viewport.left;

        const scale = self.viewport.scale;

        // scale coordinates (i.e. these are the 'pixel'-coordinates)
        const worldX = Math.ceil(x / scale.x + left);
        const worldY = Math.ceil(y / scale.y + top);

        // set the second pair of coordinates
        self.selectedArea.x2 = worldX;
        self.selectedArea.y2 = worldY;

        // draw the selection
        self.drawSelected();
      }
    }

    function mouseup(event) {
      if (self.selectionMode) {
        self.selectionMode = false;

        // enable panning again
        self.viewport.drag({factor: 1});

        // coordinates are already right, so we can adjust the area object immediately
        self.adjustArea(self.selectedArea);

        // remove the sprite, since we do not need it anymore
        self.viewport.removeChild(self.selectedSprite);

        // clamp the coordinates (-1 instead of 0 for x2 and y2 to prevent selection if the selected area is not valid)
        self.selectedArea.x1 = self.clamp(self.selectedArea.x1, 0, Math.min(self.appSizeX, self._dataset.data.length));
        self.selectedArea.x2 = self.clamp(self.selectedArea.x2, -1, Math.min(self.appSizeX, self._dataset.data.length));
        self.selectedArea.y1 = self.clamp(self.selectedArea.y1, 0, Math.min(self.appSizeY, self._dataset.data.length));
        self.selectedArea.y2 = self.clamp(self.selectedArea.y2, -1, Math.min(self.appSizeY, self._dataset.data.length));
        // self.selectedArea.x1 = self.clamp(self.selectedArea.x1, 0, self.appSizeX);
        // self.selectedArea.x2 = self.clamp(self.selectedArea.x2, -1, self.appSizeX);
        // self.selectedArea.y1 = self.clamp(self.selectedArea.y1, 0, self.appSizeY);
        // self.selectedArea.y2 = self.clamp(self.selectedArea.y2, -1, self.appSizeY);

        // all the matched edges will be put here
        const edges: Edge[] = [];

        // find edges in selected area
        for (let i = self.selectedArea.x1; i < self.selectedArea.x2; i++) {
          // loop over all the from nodes
          const nodeid = self.getKeyByValue(self.colIndexes, i);

          const incomingEdges = self._dataset.data[self.nodeIndexes[nodeid]].incomingEdges;
          for (const toNode in incomingEdges) {
            if (true) {
              const toNodeInt = parseInt(toNode, 10);
              // const toNodePosition = self.getKeyByValue(self.rowIndexes, toNodeInt);
              const toNodePosition = self.rowIndexes[toNodeInt];
              if (toNodePosition < self.selectedArea.y2 && toNodePosition >= self.selectedArea.y1) {
                edges.push({
                  from: toNodeInt,
                  to: parseInt(nodeid, 10),
                  weight: incomingEdges[toNode]
                });
              }
            }
          }
        }

        // emit select event with previously found edges
        self.selectedEvent.emit(edges);
      }
    }

// -------------------------------- dataset change / redraw heatmap ----------------------------
    this.onDatasetChange.subscribe(() => {
      if (this.viewport.children.length > 2) {
        this.viewport.removeChildren(2);
      }

      this.twenty = this._dataset.biggest * 0.2;
      this.fourty = this._dataset.biggest * 0.4;
      this.sixty = this._dataset.biggest * 0.6;
      this.eighty = this._dataset.biggest * 0.8;
      this.hundred = this._dataset.biggest;

      // Haal de heatmap van de graphics af, teken dan opnieuw
      self.graphics.removeChildren();
      this.appSizeX = this._dataset.data.length;
      this.appSizeY = this._dataset.data.length;
      // minimum size to avoid shitty behaviour
      this.appSizeX = this.appSizeX < 500 ? 500 : this.appSizeX;
      this.appSizeY = this.appSizeY < 500 ? 500 : this.appSizeY;

      this.updateLayout();
      this.drawMatrix(Math.min(this.appSizeX, this._dataset.data.length), Math.min(this.appSizeY, this._dataset.data.length));

      // Zoom pixi heatmap weer helemaal uit
      self.viewport.fit();
      self.viewport.snap(0, 0, {removeOnComplete: true, topLeft: true, time: 1});

      this.viewport.fitWidth(Math.min(this.appSizeX, this._dataset.data.length));

    });

    // Listen for animate update
    this.pApp.ticker.add(function (delta) {
    });

    this.onSelectedInputChange.subscribe(() => this.updateSelected());

  } // Einde initialization

  updateLayout() {
    switch (this._selectedLayout.toLowerCase()) {
      case 'cuthill':
          this.rowIndexes = this.colIndexes = this._dataset.orderIndices.revCuthill;
        break;
      case 'moment':
        this.rowIndexes = this._dataset.orderIndices.momentRow;
        this.colIndexes = this._dataset.orderIndices.momentCol;
        break;
      case 'degree':
        this.rowIndexes = this._dataset.orderIndices.degRow;
        this.colIndexes = this._dataset.orderIndices.degCol;
        break;
      case 'pagerank':
        this.rowIndexes = this.colIndexes = this._dataset.orderIndices.pageRank;
        break;
      default: // standard
        this.rowIndexes = this.colIndexes = Array(this.appSizeX).fill(0).map((el, idx) => idx);
        break;
    }

    this.drawMatrix(Math.min(this.appSizeX, this._dataset.data.length), Math.min(this.appSizeY, this._dataset.data.length));
    this.viewport.fit();
    this.viewport.snap(0, 0, {removeOnComplete: true, topLeft: true, time: 1});
    this.viewport.fitWidth(Math.min(this.appSizeX, this._dataset.data.length));

    // reset selection
    if (this.selectedSprite) {
      this.viewport.removeChild(this.selectedSprite);
    }
  }

  pauseScroll() {
    this.viewport.pausePlugin('wheel');
  }

  resumeScroll() {
    this.viewport.resumePlugin('wheel');
  }

// -------------------------------- connecting node link and heatmap ---------------------------

  // takes area object, and will modify it such that x1 <= x2 and y1 <= y2 (and account for rounding glitches) MODIFIES ORIGINAL OBJECT
  adjustArea(area: {x1: number, x2: number, y1: number, y2: number}) {
    // x1 has to be lower than x2, y1 needs to be lower than y2
    if (area.x1 >= area.x2) {
      area.x2 += area.x1;
      area.x1 = area.x2 - area.x1;
      area.x2 -= area.x1;

      // adjust position (floor and ceiling need to be reversed)
      area.x1--;
      area.x2++;
    }

    if (area.y1 >= area.y2) {
      area.y2 += area.y1;
      area.y1 = area.y2 - area.y1;
      area.y2 -= area.y1;

      // adjust position (floor and ceiling need to be reversed)
      area.y1--;
      area.y2++;
    }
  }

  drawSelected() {
    const area = this.selectedArea;

    // check if selectedArea has every property set
    if (area && area.hasOwnProperty('x1') && area.hasOwnProperty('x2') && area.hasOwnProperty('y1') && area.hasOwnProperty('y2')) {
      // keys exist

      // save the original area object, because we might switch some values
      const before = JSON.stringify(area);

      // adjust coordinates so it can be used for a sprite
      this.adjustArea(area);

      // if there is an old sprite, remove it first
      if (this.selectedSprite) {
        this.viewport.removeChild(this.selectedSprite);
      }

      // make new sprite
      const sprite = new PIXI.Sprite(PIXI.Texture.WHITE);

      sprite.x = area.x1;
      sprite.y = area.y1;
      sprite.width = area.x2 - area.x1;
      sprite.height = area.y2 - area.y1;
      sprite.tint = 0x01afff;
      sprite.alpha = 0.4;

      // add sprite to the scene
      this.viewport.addChild(sprite);

      // save sprite (otherwise we won't be able to remove it in the next function call)
      this.selectedSprite = sprite;

      // restore the original area
      this.selectedArea = JSON.parse(before);
    }
  }

  updateSelected() {
    // override the selectionMode just in case
    this.selectionMode = false;

    // if there is a selection currently shown, remove it first
    if (this.selectedSprite) {
      this.viewport.removeChild(this.selectedSprite);
    }

    // create a canvas that is as big as the viewport
    const canvas = document.createElement('canvas');
    canvas.width = this.appSizeX;
    canvas.height = this.appSizeY;
    const ctx = canvas.getContext('2d');
    const imagedata = ctx.getImageData(0, 0, canvas.width, canvas.height);

    // create transparent background
    for (let i = 0; i < canvas.width * canvas.height * 4; i++) {
      imagedata.data[i] = 0;
    }

    // if there is only 1 edge selected, we want to show the row and column it is in
    // if there are more edges selected, we just highlight the edges themselves
    // the number can be changed to work with more than one edges
    if (this._selectedInput.length <= 1) {
      for (const edge of this._selectedInput) {
        // set the horizontal bar
        for (let i = 0; i < this._dataset.data.length ; i++) {
          const index = imagedata.width * 4 * this.rowIndexes[edge.from] + 4 * i;

          imagedata.data[index + 0] = this._colorArraySel[0];
          imagedata.data[index + 1] = this._colorArraySel[1];
          imagedata.data[index + 2] = this._colorArraySel[2];
          imagedata.data[index + 3] = 127;
        }

        // set the vertical bar
        for (let i = 0; i < this._dataset.data.length ; i++) {
          const index = imagedata.width * 4 * i + 4 * this.colIndexes[edge.to];

          imagedata.data[index + 0] = this._colorArraySel[0];
          imagedata.data[index + 1] = this._colorArraySel[1];
          imagedata.data[index + 2] = this._colorArraySel[2];
          imagedata.data[index + 3] = 127;
        }
      }
    } else {
      // highlight the selected edges
      for (const edge of this._selectedInput) {
        const index = imagedata.width * 4 * this.rowIndexes[edge.from] + 4 * this.colIndexes[edge.to];
        imagedata.data[index + 0] = this._colorArraySel[0];
        imagedata.data[index + 1] = this._colorArraySel[1];
        imagedata.data[index + 2] = this._colorArraySel[2];
        imagedata.data[index + 3] = 127;
      }
    }


    ctx.putImageData(imagedata, 0, 0);

    // set the sprite
    this.selectedSprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(canvas));

    // add the sprite to the scene
    this.viewport.addChild(this.selectedSprite);
  }

// Making snapshots
  makeSnapshot(event: Event) {
    // Get the snapshot Data from the renderer
    const snap = this.pApp.renderer.plugins.extract.image(this.graphics);
    // Make the data a link html element
    const link = document.createElement('a');
    link.href = snap.src;
    link.download = 'HeatmapSnapshot' + Date.now();
    document.body.appendChild(link);
    // Click the link
    link.click();
    // Remove the link
    document.body.removeChild(link);
  }

// -------------------------------- drawing heatmap ----------------------------
  drawMatrix(width, height) {
    // Tekenen van effen achtergrond
    this.graphics.clear();
    this.graphics.removeChildren();
    this.graphics.beginFill(parseInt(this.backGroundColor.substr(1), 16));
    this.graphics.drawRect(0, 0, width, height);
    this.graphics.endFill();

    // create an imagedata object with the right size
    const canvas = document.createElement('canvas');
    canvas.width = this.appSizeX;
    canvas.height = this.appSizeY;
    const ctx = canvas.getContext('2d');
    const imagedata = ctx.getImageData(0, 0, width, height);

    // const canvas = this.pApp.renderer.plugins.extract.canvas();

    // Tekenen van effen achergrond
    for (let i = 0; i < 4 * this.appSizeX * this.appSizeX; i++) {
      imagedata.data[i] = 0;
    }

    if (this._dataset.mean / this._dataset.biggest < 0.10) {
      // Teken alle overige data op deze achtergrond
      for (const item of this._dataset.data) {
        // nodeId is the key, it represent the node to where the edge is going
        for (const nodeId in item.outgoingEdges) {
          // if check to make linter shut up
          if (item.outgoingEdges[nodeId] > 0) {
            const row = this.nodeIndexes[this.rowIndexes[item.nodeId]]; // parseInt(item.nodeId.substring(1), 10);
            const column = this.nodeIndexes[this.colIndexes[nodeId]]; // parseInt(nodeId.substring(1), 10);
            // console.log(row, column);
            const weight = item.outgoingEdges[nodeId];

            const index = imagedata.width * 4 * row + 4 * column;

            // color is not scaled from 0-1, so we clamp it between 0 and 255
            const color = weight / this._dataset.biggest * 255;
            const shade = weight / this._dataset.biggest * 255;

            // RGBA colors (grayscale for now)
            if (this.grayscale) {
              imagedata.data[index + 0] = this._colorArray[0];
              imagedata.data[index + 1] = this._colorArray[1];
              imagedata.data[index + 2] = this._colorArray[2];
              imagedata.data[index + 3] = Math.log2(shade) * 32;
            } else {
              imagedata.data[index + 3] = 255;
              if (weight > 0 && weight <= this.twenty) {
                imagedata.data[index + 0] = this.tinyColorArray[0];
                imagedata.data[index + 1] = this.tinyColorArray[1];
                imagedata.data[index + 2] = this.tinyColorArray[2];
              } else if (weight > this.twenty && weight <= this.fourty) {
                imagedata.data[index + 0] = this.smallColorArray[0];
                imagedata.data[index + 1] = this.smallColorArray[1];
                imagedata.data[index + 2] = this.smallColorArray[2];
              } else if (weight > this.fourty && weight <= this.sixty) {
                imagedata.data[index + 0] = this.mediumColorArray[0];
                imagedata.data[index + 1] = this.mediumColorArray[1];
                imagedata.data[index + 2] = this.mediumColorArray[2];
              } else if (weight > this.sixty && weight <= this.eighty) {
                imagedata.data[index + 0] = this.largeColorArray[0];
                imagedata.data[index + 1] = this.largeColorArray[1];
                imagedata.data[index + 2] = this.largeColorArray[2];
              } else if (weight > this.sixty && weight <= this.hundred) {
                imagedata.data[index + 0] = this.hugeColorArray[0];
                imagedata.data[index + 1] = this.hugeColorArray[1];
                imagedata.data[index + 2] = this.hugeColorArray[2];
              }
            }
          }
        }
      }
    } else {
      // Teken alle overige data op deze achtergrond
      for (const item of this._dataset.data) {
        // nodeId is the key, it represent the node to where the edge is going
        for (const nodeId in item.outgoingEdges) {
          // if check to make linter shut up
          if (item.outgoingEdges[nodeId] > 0) {
            const row = this.nodeIndexes[this.rowIndexes[item.nodeId]]; // parseInt(item.nodeId.substring(1), 10);
            const column = this.nodeIndexes[this.colIndexes[nodeId]]; // parseInt(nodeId.substring(1), 10);
            const weight = item.outgoingEdges[nodeId];

            const index = imagedata.width * 4 * row + 4 * column;

            // color is not scaled from 0-1, so we clamp it between 0 and 255
            const shade = weight / this._dataset.biggest * 255;

            // RGBA colors (grayscale for now)
            if (this.grayscale) {
              imagedata.data[index + 0] = this._colorArray[0];
              imagedata.data[index + 1] = this._colorArray[1];
              imagedata.data[index + 2] = this._colorArray[2];
              imagedata.data[index + 3] = shade;
            } else {
              imagedata.data[index + 3] = 255;
              if (weight > 0 && weight <= this.twenty) {
                imagedata.data[index + 0] = this.tinyColorArray[0];
                imagedata.data[index + 1] = this.tinyColorArray[1];
                imagedata.data[index + 2] = this.tinyColorArray[2];
              } else if (weight > this.twenty && weight <= this.fourty) {
                imagedata.data[index + 0] = this.smallColorArray[0];
                imagedata.data[index + 1] = this.smallColorArray[1];
                imagedata.data[index + 2] = this.smallColorArray[2];
              } else if (weight > this.fourty && weight <= this.sixty) {
                imagedata.data[index + 0] = this.mediumColorArray[0];
                imagedata.data[index + 1] = this.mediumColorArray[1];
                imagedata.data[index + 2] = this.mediumColorArray[2];
              } else if (weight > this.sixty && weight <= this.eighty) {
                imagedata.data[index + 0] = this.largeColorArray[0];
                imagedata.data[index + 1] = this.largeColorArray[1];
                imagedata.data[index + 2] = this.largeColorArray[2];
              } else if (weight > this.sixty && weight <= this.hundred) {
                imagedata.data[index + 0] = this.hugeColorArray[0];
                imagedata.data[index + 1] = this.hugeColorArray[1];
                imagedata.data[index + 2] = this.hugeColorArray[2];
              }
            }
          }
        }
      }
    }

    ctx.putImageData(imagedata, 0, 0);
    this.graphics.addChild(new PIXI.Sprite(PIXI.Texture.fromCanvas(canvas)));
    this.viewport.fitWidth(width);
  }

  private getKeyByValue(object, value): any {
    return Object.keys(object).find(key => object[key] === value);
  }

  private clamp(val, min, max) {
    return val < min ? min : val > max ? max : val;
  }

  redraw() {
    this.drawMatrix(Math.min(this.appSizeX, this._dataset.data.length), Math.min(this.appSizeY, this._dataset.data.length));
  }
}
