// ------------------ Imports ---------------
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Edge } from '../interfaces/edge.interface';
import { Options, ChangeContext } from 'ng5-slider';
declare var require: any;

// -------------- Angular -----------------
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
})

// ---------- Component -------------------
export class GraphComponent implements OnInit {

  private sigmaJs: any;

  private _dataset: any;
  private _selectedInput: Edge[];
  public _colorArraySel = [255, 0, 0];
  public _colorHex = '#FF0000';
  private drawingMode = 'radial';
  private edgeHideLimit = 50000;
  private dendrogram_edge_count = 0;

  public tinyColor = '#139BD4';
  public smallColor = '#8CC65B';
  public mediumColor = '#EDDA68';
  public largeColor = '#F39344';
  public hugeColor = '#FE0100';

  iterationsRender: number;
  edgeInfluence: number;
  tightClusters: boolean;
  outbound: boolean;

  onDatasetChange = new Subject();
  onSelectedInputChange = new Subject();

  // Variables for slider
  value;
  highValue;

  // Filter variable
  filter;

  // Inputs outputs for selectchange and datasetchange
  @Output() selectedEvent = new EventEmitter<any>();

  @Input() set dataset(value: any) {
    this._dataset = value;
    this._selectedInput = [];
    this.onDatasetChange.next();
  }

  @Input() set selectedInput(value: any) {
    this._selectedInput = value;
    this.onSelectedInputChange.next();
  }

  @Input() set sandboxSettings(settings: any) {
    this.iterationsRender = settings.itRen;
    this.edgeInfluence = settings.edgeWeight;
    this.outbound = settings.outAttDist;
    this.tightClusters = settings.linLogMode;
  }

  refreshSlider() {
      this.sigmaJs.refresh();
      this.sigmaJs.refresh();
      // this.updateSelected();
      this.setSelected(-1);
      this.changeSelected(-1);
  }

  refreshReshow() {
    this.sigmaJs.refresh();
  }

  resize(graph: boolean, heatmap: boolean, swapped: boolean) {

    const application = document.getElementById('sigmaContainer');

    application.classList.remove('combined');
    application.classList.remove('swapped');
    application.classList.remove('standalone');

    if (graph && heatmap) {

      if (swapped) {

        application.classList.add('swapped');

      } else {

        application.classList.add('combined');

      }

    } else if (graph && !heatmap) {

      application.classList.add('standalone');

    } else if (!graph) {

      application.classList.add('combined');

    }

    this.sigmaJs.refresh();

  }

  recolor(version: number) {
    if (version === 1) {
      for (const node of this.sigmaJs.graph.nodes()) {
        if (node.size === 5.3) {
          node.color = this.hugeColor;
        }
      }
    }

    if (version === 2) {
      for (const node of this.sigmaJs.graph.nodes()) {
        if (node.size === 4.9) {
          node.color = this.largeColor;
        }
      }
    }

    if (version === 3) {
      for (const node of this.sigmaJs.graph.nodes()) {
        if (node.size === 3.5) {
          node.color = this.mediumColor;
        }
      }
    }

    if (version === 4) {
      for (const node of this.sigmaJs.graph.nodes()) {
        if (node.size === 3) {
          node.color = this.smallColor;
        }
      }
    }

    if (version === 5) {
      for (const node of this.sigmaJs.graph.nodes()) {
        if (node.size === 2) {
          node.color = this.tinyColor;
        }
      }
    }

    this.sigmaJs.refresh();
  }

  ngOnInit() {

    // ----------------------- Import plugins ---------------------------------
    require('sigma/build/plugins/sigma.layout.forceAtlas2.min.js');
    require('sigma/src/renderers/canvas/sigma.canvas.edges.curve.js');
    require('sigma/src/renderers/canvas/sigma.canvas.edges.curvedArrow.js');
    require('sigma/build/plugins/sigma.plugins.filter.min');
    require('sigma/build/plugins/sigma.plugins.dragNodes.min.js');
    require('sigma/build/plugins/sigma.renderers.snapshot.min.js');

    /* Import the custom dendrogram renderer. */
    require('../renderers/edge-renderer.js');

    // ---------------- Initialize graph elements ----------------------

    var edgeCount = 0;
    for (let node in this._dataset.data) {
      edgeCount += Object.keys(this._dataset.data[node].outgoingEdges).length
    }

    // @ts-ignore
    this.sigmaJs = new window.sigma({
      renderer: {
        container: document.getElementById('sigmaContainer'),
        type: 'canvas'
      },
      settings: {
        drawLabels: false,
        zoomingRatio: 1.7,
        borderSize: 2,
        minNodeSize: 0,
        maxNodeSize: 0,
        minEdgeSize: 0,
        maxEdgeSize: 0,
        zoomMax: 3,
        hideEdgesOnMove: edgeCount > this.edgeHideLimit,
        batchEdgesDrawing: edgeCount > this.edgeHideLimit / 2,
        nodesPowRatio: 0.2,
        scalingMode: 'inside'
      }
    });

    // ------------------------ create filters, draglisteners ------------------------------

    // @ts-ignore
    this.filter = new window.sigma.plugins.filter(this.sigmaJs);

    // @ts-ignore
    // const dragListener = window.sigma.plugins.dragNodes(this.sigmaJs, this.sigmaJs.renderers[0]);
    let isDragged = false;
    //
    // dragListener.bind('startdrag', function(event) {
    //   isDragged = false;
    // });
    //
    // dragListener.bind('drop', function(event) {
    //   isDragged = true;
    // });

    // ------------------------- Interactivity -----------------------------------

    this.sigmaJs.bind('clickNode', e => {
      if (!e.data.captor.isDragging && !isDragged && e.data.node.id.substring(0, 1) !== 'c') {
        // Change data to new clicked node
        this.changeSelected(e.data.node.id);
        this.setSelected(e.data.node.id);
        isDragged = false;
      } else if (!e.data.captor.isDragging && !isDragged) {
        // Select cluster and draw again with that cluster expanded
        const clusterSelection = parseInt(e.data.node.id.substring(1, e.data.node.id.length), 10);
        if (this.sigmaJs.graph.clusterSelection.includes(clusterSelection)) {
          delete this.sigmaJs.graph.clusterSelection[this.sigmaJs.graph.clusterSelection.indexOf(clusterSelection)];
        } else {
          this.sigmaJs.graph.clusterSelection.push(clusterSelection);
        }
        this.drawClusteredGraph(this.sigmaJs.graph.clusterSelection);
        isDragged = false;
      }
    });

    // Unselecting
    this.sigmaJs.bind('clickStage', e => {
      if (!e.data.captor.isDragging && !isDragged) {

        for (const edge of this.sigmaJs.graph.edges()) {
          const opacity = edge.opacity;
          edge.color = `rgba(0,0,0,${opacity})`;
          }
        // Pass -1 as a node id since it will never be found as a real node
        // Which will make the changeSelected event an array length 0
        this.changeSelected(-1);
        // Everything will be colored back to normal
        this.setSelected(-1);
      }
    });

    // --------------------------- Dataset change -------------------------------------
    this.onDatasetChange.subscribe(() => {

      if (this.drawingMode === 'dendrogram' && typeof(this._dataset.hierarchy) === 'undefined') {
        this.drawingMode = 'radial';
      }

      var edgeCount = 0;
      for (let node in this._dataset.data) {
        edgeCount += Object.keys(this._dataset.data[node].outgoingEdges).length
      }
      this.sigmaJs.settings('hideEdgesOnMove', edgeCount > this.edgeHideLimit);
      this.sigmaJs.settings('batchEdgesDrawing', edgeCount > this.edgeHideLimit / 2);
      // Redraw graph
      this.drawGraph(this.drawingMode);
    });


    this.onSelectedInputChange.subscribe(() => {
        this.updateSelected();
    });

    this.drawGraph(this.drawingMode);

  }

  // ----------------------- Events for selecting new node ----------------------

  setSelected(nodeId) {
    // Color all nodes blue
    for (const node of this.sigmaJs.graph.nodes()) {
      if (nodeId === node.id) {
        node.color = this._colorHex;
      } else {
        node.color = node.ogcolor;
      }
    }

    const self = this;

    /* Don't hide dendrogram edges. */
    if (this.drawingMode === 'dendrogram') {
      this.drawDendrogramPath(nodeId);
      return;
    }

    this.filter.undo();
    this.filter
      .edgesBy(function(edge) {
        return edge.weight >= self.value;
      })
      .edgesBy(function(edge) {
        return edge.weight <= self.highValue;
      })
      .apply();

    this.sigmaJs.refresh();
  }

  // ---------------- Interactivity 2 -------------------

  selectEdge(from, to) {

    if (this._dataset.symmetric) {

      let actualFrom = from;
      let actualTo = to;

      if (from < to) {
        actualFrom = to;
        actualTo = from;
      }

      for (const edge of this.sigmaJs.graph.edges()) {
        let opacity = edge.opacity;
          // tslint:disable-next-line:triple-equals
          if (edge.source == actualFrom && edge.target == actualTo) {
            if (opacity < 0.2) {
              opacity = 0.2;
            }
            edge.color = `rgba(${this._colorArraySel[0]},${this._colorArraySel[1]},${this._colorArraySel[2]},1)`;
          } else {
            edge.color = `rgba(0,0,0,${opacity})`;
          }
      }
    } else {
      for (const edge of this.sigmaJs.graph.edges()) {
        let opacity = edge.opacity;
        // tslint:disable-next-line:triple-equals
        if (edge.source == from && edge.target == to) {
          if (opacity < 0.2) {
            opacity = 0.2;
          }
          edge.color = `rgba(${this._colorArraySel[0]},${this._colorArraySel[1]},${this._colorArraySel[2]},1)`;
        } else {
          edge.color = `rgba(0,0,0,${opacity})`;
        }
      }
    }

    this.sigmaJs.refresh();
  }

  makeSnapshot() {
    // Get the data url from the renderer
    const dataUrl = this.sigmaJs.renderers[0].snapshot();
    // Make the snapshot and download it
    this.sigmaJs.renderers[0].snapshot({
      download: true,
      format: 'png',
      background: 'white',
      labels: true,
      filename: 'GraphSnapshot' + Date.now()
    });
  }

  // toggle forece
  toggleForce() {
    if (!this.sigmaJs.isForceAtlas2Running()) {
      this.sigmaJs.startForceAtlas2({
        linLogMode: this.tightClusters,
        iterationsPerRender: this.iterationsRender,
        outboundAttractionDistribution: this.outbound,
        edgeWeightInfluence: this.edgeInfluence,
        worker: true,
        autoStop: true,
        backGround: true,
        scaleRatio: 10,
        gravity: 3,
       });
    } else {
      this.sigmaJs.killForceAtlas2();
    }
  }

  toggleCluster(clustered: boolean) {
    if (clustered) {
      this.drawGraph(this.drawingMode);
    } else {
      this.drawClusteredGraph([]);
    }
  }

  redrawArch() {
    if (this.sigmaJs.isForceAtlas2Running()) {
      this.sigmaJs.killForceAtlas2();
    }
    this.drawGraph('arch');
  }

  // Redraw with random node locations
  redrawRandom() {
    if (this.sigmaJs.isForceAtlas2Running()) {
      this.sigmaJs.killForceAtlas2();
    }
    this.drawGraph('random');
  }

  // Redraw as the standard circular form
  redrawCircle() {
    if (this.sigmaJs.isForceAtlas2Running()) {
      this.sigmaJs.killForceAtlas2();
    }
    this.drawGraph('radial');
  }

  redrawDendro() {
    if (this.sigmaJs.isForceAtlas2Running()) {
      this.sigmaJs.killForceAtlas2();
    }
    this.drawGraph('dendrogram');
  }

  // Change selected date of sharingservice
  changeSelected(from) {
    const fromNumber = parseInt(from, 10);
    // find all the edges that are selected
    const tmp = this._dataset.data.find(node => node.nodeId === fromNumber.toString());

    const edges: Edge[] = [];

    // Only append data if node exists with given node id
    if (tmp) {
      edges.push({
        from: fromNumber,
        to: -1,
        weight: -1
      })

      // tslint:disable-next-line: forin
      for (const toId in tmp.incomingEdges) {
        edges.push({
          from: <number><any>toId,
          to: fromNumber,
          weight: <number><any>tmp.incomingEdges[toId]
        });
      }

      // tslint:disable-next-line: forin
      for (const toId in tmp.outgoingEdges) {
          edges.push({
          from: fromNumber,
          to: <number><any>toId,
          weight: <number><any>tmp.outgoingEdges[toId]
        });
      }
    }
    this.selectedEvent.emit(edges);
  }

  // Update the current selection with the new data
  updateSelected() {

    /* Don't hide dendrogram edges. */
    if (this.drawingMode === 'dendrogram') {
      return;
    }

      // if nothing is selected draw as usual
      if (this._selectedInput.length === 0) {

        const self = this;
        this.filter.undo();
        this.filter
          .edgesBy(function(edge) {
            return edge.weight >= self.value;
          })
          .edgesBy(function(edge) {
            return edge.weight <= self.highValue;
          })
          .apply();

      // Else apply appropriate filter
      } else {
        // make object to improve efficiency in the filter
        const select = {};

        for (const edge of this._selectedInput) {
          if (edge.weight >= this.value && edge.weight <= this.highValue ) {
            if (!select[edge.from]) {
              select[edge.from] = {};
            }
            select[edge.from][edge.to] = 1;
          }
        }

        if (this._dataset.symmetric) {
          for (const edge of this._selectedInput) {
            if (edge.weight >= this.value && edge.weight <= this.highValue ) {
              if (!select[edge.to]) {
                select[edge.to] = {};
              }
              select[edge.to][edge.from] = 1;
            }
          }
        }

        this.filter.undo();
        this.filter.edgesBy(edge => {
          return select[edge.source] && select[edge.source][edge.target];
        }, 'selection')
        .apply();
      }
  }


  // ----------------------- Drawing the whole graph ------------

  drawGraph(mode) {
    // clear the graph before adding nodes to it
    this.sigmaJs.graph.clear();
    this.sigmaJs.graph.clustered = false;

    this.drawingMode = mode;

    if (this.drawingMode === 'dendrogram') {
      this.drawDendrogram(0);
      return;
    }

    let random = mode === 'random';
    let radial = mode === 'radial';
    let arch = mode === 'arch';

    let sortedDegrees = [];

    if (arch) {
      let degrees = [];

      for (const i in this._dataset.data) {
        degrees[i] = this._dataset.data[i].outdegree;
      }

      let degIndex = 0;

      while (Math.max.apply(null, degrees) > -1) {
        let deg = Math.max.apply(null, degrees);
        let degId = degrees.indexOf(deg);
        sortedDegrees[degIndex] = degId
        degrees[degId] = -1;
        degIndex++;
      }

    }

    // ------------------- Draw Nodes --------------------------

    for (const item of this._dataset.data) {
      let size = 1;
      const degree = item.outdegree;
      const biggest = this._dataset.maxout;

      let color = '#000000';
      let degIndex = (arch) ? sortedDegrees.indexOf(parseInt(item.nodeId)) : 0;


      if (degree <= 0.05 * biggest) {
        size = 2;
        color = this.tinyColor;
      } else if (degree <= 0.1 * biggest) {
        size = 3;
        color = this.smallColor;
      } else if (degree <= 0.3 * biggest) {
        size = 3.5;
        color = this.mediumColor;
      } else if (degree <= 0.7 * biggest) {
        size = 4.9;
        color = this.largeColor;
      } else {
        size = 5.3;
        color = this.hugeColor;
      }

      this.sigmaJs.graph.addNode({
        // Node id is based on drawing order, and thus the order in the original dataset
        id: item.nodeId,
        label: item.name,
        // Make it a circle
        x: (random) ? Math.random() : (arch) ? degIndex : item.circleX, // random location or not
        y: (random) ? Math.random() : (arch) ? 0 : item.circleY,
        size: size,
        color : color,
        indegree: item.indegree,
        outdegree: item.outdegree,
        cuthillMcKeeIndex: item.cuthillMcKeeIndex,
        ogcolor: color,
      });
    }

    // ------------------- Draw Edges ----------------------------------

    // --------------------- non-clustered / symmetric -----------------------------

    const datasetLength = this._dataset.data.length;
    let currentRow = 0;
    const biggest = this._dataset.biggest;
    const sizeArray = [0.75, 1, 1.5, 2, 2.5, 3];

    if (this._dataset.symmetric) {
      for (const node of this._dataset.data) {
      // tslint:disable-next-line: forin
        for (const index in node.outgoingEdges) {

          // Give symmetric edges same id

          const originalIndex = parseInt(index, 10);
          const moduleIndex = originalIndex % datasetLength;

          if (moduleIndex === 0 || originalIndex <= currentRow) {
            let edgeId = '';

            if (node.nodeId < index) {
              edgeId = `${node.nodeId}-${index}`;
            } else {
              edgeId = `${index}-${node.nodeId}`;
            }

            // Set opacity
            const opacity = Math.ceil(20 * node.outgoingEdges[index] / biggest) / 20;
            const size = sizeArray[Math.floor(Math.floor(20 * opacity) / 4)];


            // Check if edge is already added to graph
            this.sigmaJs.graph.addEdge({
              id: edgeId,
              source: node.nodeId,
              target: index,
              color: `rgba(0,0,0,${opacity})`,
              type: 'curve',
              size: size,
              opacity: opacity,
              weight: node.outgoingEdges[index]
            });
          }
        }
        currentRow++;
      }
    } else {
    // ---------------------------- nonclustered / not symmetric -----------------------------
      for (const node of this._dataset.data) {
      // tslint:disable-next-line: forin
        for (const index in node.outgoingEdges) {

          // Set opacity
          const opacity = Math.ceil(20 * node.outgoingEdges[index] / biggest) / 20;
          const size = sizeArray[Math.floor(Math.floor(20 * opacity) / 4)];

          this.sigmaJs.graph.addEdge({
            id: `${node.nodeId}-${index}`,
            source: node.nodeId,
            target: index,
            color: `rgba(0,0,0,${opacity})`,
            type: 'curvedArrow',
            size: size,
            opacity: opacity,
            weight: node.outgoingEdges[index]
          });
        }
      }
    }

    const self = this;

    // Apply filter
    this.filter
      .edgesBy(function(edge) {
        return edge.weight >= self.value;
      })
      .edgesBy(function(edge) {
        return edge.weight <= self.highValue;
      })
      .apply();

    // Finally, let's ask our sigma instance to refresh:
    this.sigmaJs.refresh();
  }

  // ----- Drawing the graph, clustered --------------------------------------------------

  drawClusteredGraph(clusterSelection) { // Parameter (random) is a boolean that dictates whether nodes have random locations or not.
    // clear the graph before adding nodes to it
    this.sigmaJs.graph.clear();
    this.sigmaJs.graph.clustered = true;
    this.sigmaJs.graph.clusterSelection = clusterSelection;

    // Draw Cluster indicators
    const clusterSizes = this._dataset.clusterSizes;
    const clusterCount = clusterSizes.length;
    const clustersX = [];
    const clustersY = [];
    for (let c = 0; c < clusterCount; c++) {
      clustersX[c] = 0.7 * Math.cos(c * (2 * Math.PI / clusterCount));
      clustersY[c] = 0.7 * Math.sin(c * (2 * Math.PI / clusterCount));
      this.sigmaJs.graph.addNode({
        id: 'c' + c,
        label: 'Cluster ' + c,
        x: clustersX[c],
        y: clustersY[c],
        size: 8,
        color: '#FF007F',
        ogcolor: '#FF007F'
      });
    }

    // ------------------- Draw Individual Nodes --------------------------

    const clusterIndex = []; // clusterIndex[i] = How many nodes have already been drawn for cluster i
    for (const item of this._dataset.data) {
      const c = item.cluster;
      if (clusterSelection.includes(c)) {
        let size = 1;
        const degree = item.outdegree;
        const biggest = this._dataset.maxout;

        let color = '#000000';

        if (degree <= 0.05 * biggest) {
          size = 2;
          color = this.tinyColor;
        } else if (degree <= 0.1 * biggest) {
          size = 3;
          color = this.smallColor;
        } else if (degree <= 0.3 * biggest) {
          size = 3.5;
          color = this.mediumColor;
        } else if (degree <= 0.7 * biggest) {
          size = 4.9;
          color = this.largeColor;
        } else {
          size = 5.3;
          color = this.hugeColor;
        }

        let randomX = 1;
        let randomY = 1;
        while (Math.sqrt(randomX ** 2 + randomY ** 2) > 0.25 || Math.sqrt(randomX ** 2 + randomY ** 2) < 0.02) {
          randomX = Math.random() * 0.5 - 0.25;
          randomY = Math.random() * 0.5 - 0.25;
        }

        this.sigmaJs.graph.addNode({
          // Node id is based on drawing order, and thus the order in the original dataset
          id: item.nodeId,
          label: item.name,
          // Make it a circle
          x: clustersX[c] + randomX,
          y: clustersY[c] + randomY,
          size: size,
          color : color,
          indegree: item.indegree,
          outdegree: item.outdegree,
          cuthillMcKeeIndex: item.cuthillMcKeeIndex,
          ogcolor: color,
          cluster: c,
        });

        clusterIndex[c] += 1;
      }
    }

    // ------------------- Draw Edges ----------------------------------

    // -------- Symmetric --------------------------------------

    const datasetLength = this._dataset.data.length;
    let currentRow = 0;
    const biggest = this._dataset.biggest;
    const sizeArray = [0.75, 1, 1.5, 2, 2.5, 3];

    if (this._dataset.symmetric) {
      for (const node of this._dataset.data) {
        if (clusterSelection.includes(node.cluster)) {
          for (const index in node.outgoingEdges) {
            if (clusterSelection.includes(this._dataset.data[parseInt(index, 10)].cluster)) {

              const originalIndex = parseInt(index, 10);
              const moduleIndex = originalIndex % datasetLength;

              if (moduleIndex === 0 || originalIndex <= currentRow) {
                let edgeId = '';

                if (node.nodeId < index) {
                  edgeId = `${node.nodeId}-${index}`;
                } else {
                  edgeId = `${index}-${node.nodeId}`;
                }

                // Set opacity
                const opacity = Math.ceil(20 * node.outgoingEdges[index] / biggest) / 20;
                const size = sizeArray[Math.floor(Math.floor(20 * opacity) / 4)];


                // Check if edge is already added to graph
                this.sigmaJs.graph.addEdge({
                  id: edgeId,
                  source: node.nodeId,
                  target: index,
                  color: `rgba(0,0,0,${opacity})`,
                  type: 'curve',
                  size: size,
                  opacity: opacity,
                  weight: node.outgoingEdges[index]
                });
              }
            }
          }
          currentRow++;
        }
      }
      // -------- Non-Symmetric --------------------------------------
    } else {
      for (const node of this._dataset.data) {
        if (clusterSelection.includes(node.cluster)) {
          for (const index in node.outgoingEdges) {
            if (clusterSelection.includes(this._dataset.data[parseInt(index, 10)].cluster)) {

              // Set opacity
              const opacity = Math.ceil(20 * node.outgoingEdges[index] / biggest) / 20;
              const size = sizeArray[Math.floor(Math.floor(20 * opacity) / 4)];

              this.sigmaJs.graph.addEdge({
                id: `${node.nodeId}-${index}`,
                source: node.nodeId,
                target: index,
                color: `rgba(0,0,0,${opacity})`,
                type: 'curvedArrow',
                size: size,
                opacity: opacity,
                weight: node.outgoingEdges[index]
              });
            }
          }
        }
      }
    }

    const self = this;

    // Apply filter
    this.filter
      .edgesBy(function(edge) {
        return edge.weight >= self.value;
      })
      .edgesBy(function(edge) {
        return edge.weight <= self.highValue;
      })
      .apply();

    // Finally, let's ask our sigma instance to refresh:
    this.sigmaJs.refresh();
  }


  private getSizeColor(degree, biggest) {

    let size, color;
    if (degree <= 0.05 * biggest) {
      size = 2;
      color = this.tinyColor;
    } else if (degree <= 0.1 * biggest) {
      size = 3;
      color = this.smallColor;
    } else if (degree <= 0.3 * biggest) {
      size = 3.5;
      color = this.mediumColor;
    } else if (degree <= 0.7 * biggest) {
      size = 4.9;
      color = this.largeColor;
    } else {
      size = 5.3;
      color = this.hugeColor;
    }

    return [size, color];
  }

  /**
   * Draw a dendrogram with SigmaJS.
   * @author M.H.M. Leus (1315366)
   * @param linktype Which of slink and clink is used to produce the dendrogram.
  */
  drawDendrogram(linktype = 0) {

    const graph = this.sigmaJs.graph;

    graph.clear();
    graph.clustered = false;

    /* Get the correct linkage results. */
    let linkage;
    switch(linktype) {
      case 0:
        linkage = this._dataset.hierarchy.slink;
        break;
      case 1:
        linkage = this._dataset.hierarchy.clink;
        break;
      default:
        linkage = this._dataset.hierarchy.slink;
    }

    const biggest = this._dataset.maxout;

    const xoff = 3 * 6;

    const max_h = linkage.heights[linkage.lookup[0]];
    const min_h = linkage.min_height || max_h;

    const sS = Math.sqrt(this._dataset.data.length);
    const H = Math.min(sS / 5, 15);

    let factor = (sS - H);

    if ((max_h - min_h) / min_h >= 0.05) {
      factor /= (max_h - min_h);
    }

    const yoff = H + factor * min_h;

    factor *= 5;


    /* Add all normal nodes to the graph. */
    for (let i = 0; i < this._dataset.data.length - 1; ++i) {

      /* Which node is at the ith position? */
      const node = linkage.lookup[i];

      /* Find the index of node's parent. */
      const parent_idx = linkage.order[linkage.parents[node]];

      const item = this._dataset.data[node];

      let size, color;
      [size, color] = this.getSizeColor(item.outdegree, biggest);

      /* Create the node. */
      graph.addNode({
        id: node.toString(),
        label: item.name,
        x: i*xoff,
        y: 0,
        size: size,
        color: color,
        ogcolor: color,
      });

      /* Create the helper node. */
      graph.addNode({
        id: node.toString() + 'd',
        x: parent_idx * xoff,
        y: -(linkage.heights[node] * factor + yoff),
      });

      /* Add a dendrogram edge between the node and its helper. */
      graph.addEdge({
        id: node.toString(),
        source: node,
        target: node.toString() + 'd',
        type: 'dendrogram',
        color: color,
        weight: 0,
      });

    }

    const lastNode = this._dataset.data.length - 1;
    const lastNodeItem = this._dataset.data[lastNode];

    let size, color;
    [size, color] = this.getSizeColor(lastNodeItem.outdegree, biggest);

    graph.addNode({
      id: lastNode.toString(),
      label: lastNodeItem.name,
      x: lastNode * xoff,
      y: 0,
      size: size,
      color: color,
      ogcolor: color,
    });

    /* Add a special node to connect the last node to. */
    graph.addNode({
      id: lastNode + 'd',
      x: lastNode * xoff,
      y: -1.5 * (max_h * factor + yoff),
    });

    /* Add an edge from last node to the special node. */
    graph.addEdge({
      id: lastNode.toString(),
      source: lastNode,
      target: lastNode + 'd',
      color: color,
      weight: 0,
    });

    /* Each data node always has exactly one edge associated with it. */
    this.dendrogram_edge_count = this._dataset.data.length;

    this.sigmaJs.refresh();

  }

  /**
   * Draw the path through the dendrogram from the node start
   * @author M.H.M. Leus (1315366)
   * @param start The starting node of the path.
  */
  drawDendrogramPath(start, linktype = 0) {

    if (start < 0) {
      this.drawDendrogram(linktype);
      return;
    }

    const graph = this.sigmaJs.graph;

    /* Get the correct linkage results. */
    let linkage;
    switch(linktype) {
      case 0:
        linkage = this._dataset.hierarchy.slink;
        break;
      case 1:
        linkage = this._dataset.hierarchy.clink;
        break;
      default:
        linkage = this._dataset.hierarchy.slink;
    }

    /* Remove all edges */
    for (let i = 0; i < this.dendrogram_edge_count; ++i) {
      graph.dropEdge(i.toString());
    }

    /* Parse the start node's id to an integer format. */
    start = parseInt(start, 10);

    /* Get the colour of the path. */
    const color = this.getSizeColor(this._dataset.data[start].outdegree, this._dataset.maxout)[1];

    /* Get the id of the last (rightmost) node in the dendrogram. */
    const lastNode = this._dataset.data.length - 1;

    /* Keep track of the amount of edges. */
    let edgeCount = 0;

    /* Can only draw the first special edge if we haven't clicked the last node. */
    if (start < lastNode) {
      graph.addEdge({
        id: edgeCount.toString(),
        source: start,
        target: start + 'd',
        type: 'dendrogram',
        color: color,
        width: 2,
        weight: 0,
      });

      edgeCount += 1;
    } else {
      /* Add an edge from last node to the special node. */
      graph.addEdge({
        id: edgeCount.toString(),
        source: lastNode,
        target: lastNode + 'd',
        color: color,
        weight: 0,
      });

      edgeCount += 1;
    }

    /* Draw the remaining edges. */
    while (start < lastNode) {

      const parent = linkage.parents[start];

      /* Add a default coloured edge from start's parent to start's helper node. */
      graph.addEdge({
        id: edgeCount.toString(),
        source: parent,
        target: start + 'd',
        weight: 0,
      });

      edgeCount += 1;

      /* SigmaJS limitation requires node size > 0.*/
      graph.nodes(parent + 'd').size = 0.1;

      graph.addEdge({
        id: edgeCount.toString(),
        source: start + 'd',
        target: parent + 'd',
        type: 'dendrogram',
        color: color,
        width: 2,
        weight: 0,
      });

      edgeCount += 1;
      start = parent;
    }

    this.dendrogram_edge_count = edgeCount;

    this.sigmaJs.refresh();
  }

}
