import { Injectable } from '@angular/core';
import { HttpRequest, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CanvasService {

  constructor(private http: HttpClient) { }

  /**
   * Will return a list of available canvas-objects
   */
  public getStoredItems() {
    return this.http.get(environment.apiUrl + 'canvas/', {});
  }

  /**
   * Retreives the canvas-data from the backend (will have status 400 if the file does not exists)
   * @param filename The name of the file where the canvas-object is stored in
   */
  public getCanvas(filename: string) {
    return this.http.get(environment.apiUrl + `canvas/${filename}`, {});
  }

  /**
   * Saves a canvas on the backend, so it can be retreived later
   * @param filename the name that the file will have
   * @param content the file contents (canvas data/object)
   */
  public saveCanvas(filename: string, content: object) {
    return this.http.post(environment.apiUrl + `canvas/upload/${filename}`, {
      content: content
    });
  }
  /**
   * Deletes a stored canvas on the backend (for example, if it is outdated)
   * @param filename The name of the file that will get deleted
   */
  public deleteCanvas(filename: string) {
    return this.http.delete(environment.apiUrl + `canvas/${filename}`);
  }
}
