import { Injectable } from '@angular/core';
import { HttpRequest, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatasetService {

  constructor(private http: HttpClient) { }

  /**
   * Returns the availble datasets
   */
  public getDatasets(): Observable<string[]> {
    return this.http.get<string[]>(environment.apiUrl + 'datasets/', {});
  }

  /**
   * Returns a specific dataset, if it exists
   * @param dataset The dataset that will be fetched
   */
  public getDataset(dataset: string) {
    return this.http.request(new HttpRequest('GET', environment.apiUrl + `datasets/${dataset}`, {}, {
      reportProgress: true
    }));
  }

  public uploadDataset(data: FormData): Observable<any> {
    return this.http.request(new HttpRequest('POST', environment.apiUrl + `datasets/upload`, data, {
      reportProgress: true
    }));
  }
}
