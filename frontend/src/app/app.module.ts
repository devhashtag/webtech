import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeatmapComponent } from './heatmap/heatmap.component';
import { GraphComponent } from './graph/graph.component';
import { BackgroundComponent } from './background/background.component';

import { DatasetService } from './services/dataset.service';
import { DatasetsComponent } from './datasets/datasets.component';
import { ParentComponent } from './parent/parent.component';
import { Ng5SliderModule } from 'ng5-slider';
import { ColorPickerModule } from '../plugins/ngx-color-picker-master/src';
import { SidebarModule  } from 'ng-sidebar';

@NgModule({
  declarations: [
    AppComponent,
    HeatmapComponent,
    GraphComponent,
    DatasetsComponent,
    ParentComponent,
    BackgroundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng5SliderModule,
    ColorPickerModule,
    SidebarModule.forRoot()
  ],
  providers: [
    DatasetService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
