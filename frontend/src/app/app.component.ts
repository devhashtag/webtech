import { Component, ViewChild } from '@angular/core';
import { ParentComponent } from '../app/parent/parent.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  @ViewChild(ParentComponent) parentComponent: ParentComponent;

  ngOnInit() {
    this.HideVis();
    document.getElementById("StartButton").style.display = 'none';
    };

  showStart() {
    document.getElementById("StartButton").style.display = 'block';
  }

  ShowVis() {
    document.getElementById("Vis").style.display = 'block';
    document.getElementById("StartButton").style.display = 'none';
    document.getElementById("DatasetDiv").style.display = 'none';
    document.getElementById("Flavour").style.display = 'none';
    document.getElementById("Title").style.display = 'none';
    document.getElementById("matrixBackground").style.display = 'none';
    this.parentComponent.refreshSlider();
  }

  HideVis() {
    document.getElementById("Vis").style.display = 'none';
    document.getElementById("StartButton").style.display = 'block';
    document.getElementById("DatasetDiv").style.display = 'block';
    document.getElementById("Flavour").style.display = 'block';
    document.getElementById("Title").style.display = 'block';
    document.getElementById("matrixBackground").style.display = 'block';
  }
}
