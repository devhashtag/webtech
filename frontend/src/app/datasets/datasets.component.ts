import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpEventType, HttpRequest, HttpClient } from '@angular/common/http';
import { DatasetService } from '../services/dataset.service';
import { $ } from 'protractor';
import { start } from 'repl';

@Component({
  selector: 'app-datasets',
  templateUrl: './datasets.component.html',
  styleUrls: ['./datasets.component.css']
})
export class DatasetsComponent implements OnInit {

  constructor(private datasetService: DatasetService, private fb: FormBuilder, private http: HttpClient) { }

  @Output() datasetEvent = new EventEmitter();

  @Output() loadedEvent = new EventEmitter();
  // contains the formdata
  form: FormGroup;

  // keeps track of upload progress
  progress = 0;
  isLoaded = false;
  boolProgress = false;
  isDone = false;
  maxFileSize = 25; // in MB
  isChanged = false;

  // contains all the available datasets
  datasets: string[] = [];

  // contains the chosen dataset
  dataset: string;

  // contains the selected file for upload
  uploadfile;

  loading = false;

  ngOnInit() {
    // initialize form with a formgroup object
    this.form = this.fb.group({
      dataset: null
    });

    this.isChanged = false;

    // fetch available datasets
    this.getDatasets();
  }

  getDatasets() {
    this.datasetService.getDatasets().subscribe(res => {
      this.datasets = res;
    }, err => {
      console.error(err);
    });
  }

  onFileChange(event) {
    this.isChanged = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const fileSize = file.size;
      if (fileSize < this.maxFileSize * 1000000) {
        this.form.get('dataset').setValue(file);
        this.uploadfile = file;
      } else {
        alert("File too large (max file size: " + this.maxFileSize + "MB)");
      }
    }
  }

  submit() {
    if (!this.isChanged) {
      alert("No file selected");
    } else {
      let fileType = this.uploadfile.name.split('.');
      fileType = fileType[fileType.length - 1];
      if (fileType !== 'csv') {
        alert("Wrong filetype selected");
      } else {
        const formdata = new FormData();

        formdata.append('uploadInput', this.form.get('dataset').value);

        this.progress = 0;
        this.boolProgress = true;
        this.datasetService.uploadDataset(formdata).subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          }

          if (event.type === HttpEventType.Response) {
            this.progress = 100;
            this.boolProgress = false;
            this.isDone = true;
            let self = this;
            setTimeout(function () {
              self.isDone = false;
            }, 2000);
          }
          this.getDatasets();
        }, err => {
          console.error('Error occured: ', err);
        });
      }
    }
  }

  selectDataset(dataset: string) {
    this.loading = true;
    this.datasetService.getDataset(dataset).subscribe((res: any) => {
      if (res.type === HttpEventType.Response) {
        this.datasetEvent.emit(res.body.dataset);
        this.loading = false;
        this.isLoaded = true;
        this.loadedEvent.emit();
      }
    }, err => {
      console.error('cannot load dataset: ', err);
    });
  }

}
