
/**
 * @author M.H.M. Leus (1315366)
 */
;(function() {
    'use strict';
  
    sigma.utils.pkg('sigma.canvas.edges');
  
    /**
     * This edge renderer will display edges as arrows going from the source node
     *
     * @param  {object}                   edge         The edge object.
     * @param  {object}                   source node  The edge source node.
     * @param  {object}                   target node  The edge target node.
     * @param  {CanvasRenderingContext2D} context      The canvas context.
     * @param  {configurable}             settings     The settings function.
     */
    sigma.canvas.edges.dendrogram = function(edge, source, target, context, settings) {
        const prefix = settings('prefix') || '';
        
        context.strokeStyle = edge.color || settings('defaultNodeColor');
        context.lineWidth = edge.width || 1;
        context.beginPath();
    
        context.moveTo(source[prefix + 'x'], source[prefix + 'y']);
        context.lineTo(source[prefix + 'x'], target[prefix + 'y']);
        context.lineTo(target[prefix + 'x'], target[prefix + 'y']);
        
        context.stroke();
    
    };

})();