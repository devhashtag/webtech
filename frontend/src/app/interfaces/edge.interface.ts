export interface Edge {
    from: number;
    to: number;
    weight: number;
}
