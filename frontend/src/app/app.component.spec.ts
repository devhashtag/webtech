import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {HeatmapComponent} from './heatmap/heatmap.component';
import {GraphComponent} from './graph/graph.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeatmapComponent,
        GraphComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
