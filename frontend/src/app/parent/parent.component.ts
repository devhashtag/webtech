import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {GraphComponent} from '../graph/graph.component';
import {HeatmapComponent} from '../heatmap/heatmap.component';
import {ChangeContext, Options} from 'ng5-slider';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  // contains the dataset and visibility of the graphs
  public dataset: any = undefined;
  public selectedData = {};

  public _opened = false;
  public heatmap = false;
  public graph = true;
  public swapped = false;
  public counter = 0;
  public datasetsize;

  public hasHierarchy = false;
  public hasCluster = false;

  public tinyColor = '#139BD4';
  public smallColor = '#8CC65B';
  public mediumColor = '#EDDA68';
  public largeColor = '#F39344';
  public hugeColor = '#FE0100';

  public tinyColor2 = '#139BD4';
  public smallColor2 = '#8CC65B';
  public mediumColor2 = '#EDDA68';
  public largeColor2 = '#F39344';
  public hugeColor2 = '#FE0100';

  public backGroundColor = '#FFFFFF';

  private currentMode = 'grayscale';
  private currentModeBool = true;

  private fromNode;
  private toNode;
  private weight;
  private degree;
  private cluster;
  private force;

  public layouts = ['Standard', 'Cuthill', 'Moment', 'Degree', 'Pagerank'];
  public selectedLayout = 'Cuthill';

  public colorPickerSel = 'rgba(255,0,0,1)';
  public colorPickerHeat = 'rgba(0,0,0,1)';
  public colorArray = [0, 0, 0];

  private singleEdge;
  private singleNode;

  // Variables for slider
  value;
  highValue;

  options: Options = {
    floor: 0,
    ceil: 100,
    getPointerColor : (value: number): string => 'green',
    getSelectionBarColor : (value: number): string => 'green',
  };

  sandboxSettings: object = {};

  private clusterStatus = false;

  public selected = false;

  manualRefresh: EventEmitter<void> = new EventEmitter<void>();

  @Output() hideVisuals = new EventEmitter();

  @ViewChild(GraphComponent) graphMethods: GraphComponent;
  @ViewChild(HeatmapComponent) heatmapMethods: HeatmapComponent;

  constructor() { }

  ngOnInit() {
    document.getElementById("sidebar").addEventListener("mouseover", mouseOver);
    document.getElementById("sidebar").addEventListener("mouseout", mouseOut);

    const self = this;

    function mouseOver() {
      self.heatmapMethods.pauseScroll();
    }


    function mouseOut() {
      self.heatmapMethods.resumeScroll();
    }

    this.datasetsize = Object.keys(this.selectedData).length;

    // Create row css cus angular lol
    const css = '.dataTable tr:hover{ text-decoration: underline; background-color: #98ca7d} .dataTable tr{cursor: pointer;  text-align: right;} ' +
      '.dataTable th {text-overflow: ellipsis; white-space: nowrap; overflow: hidden; } .first {width:40%;} .second {width:20%; }';
    const style = document.createElement('style');
    if ((<any>style).styleSheet) {
      (<any>style).styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    document.getElementsByTagName('head')[0].appendChild(style);

  }

  swapVis(){
    this.swapped = !this.swapped;
    this.graphMethods.resize(this.graph, this.heatmap, this.swapped);
    this.heatmapMethods.resize(this.heatmap, this.graph, this.swapped);
  }

  increment() {
    this.counter++;
    const from = this.selectedData[this.counter].from;
    const to = this.selectedData[this.counter].to;
    this.fromNode = this.dataset.data[from].name + ' (' + from + ')';
    this.toNode = this.dataset.data[to].name + ' (' + to + ')';
    this.weight = this.selectedData[this.counter].weight;
    this.graphMethods.selectEdge(from, to);
  }

  decrement() {
    this.counter--;
    const from = this.selectedData[this.counter].from;
    const to = this.selectedData[this.counter].to;
    this.fromNode = this.dataset.data[from].name + ' (' + from + ')';
    this.toNode = this.dataset.data[to].name + ' (' + to + ')';
    this.weight = this.selectedData[this.counter].weight;
    this.graphMethods.selectEdge(from, to);
  }

  tableSelect(event: Event) {
    let row = (<any>event).path["0"].id;
    if (row.substring(0,3) === 'row') {
      let id = row.substring(3, row.length);
      this.graphMethods.changeSelected(id);
      this.graphMethods.setSelected(id);
    }
  }

  searchQuery(event: Event) {
    const query = (<HTMLInputElement>event.target).value.toLowerCase();
    const table = document.getElementById("dataTable");
    const tableRows = table.getElementsByTagName("tr");

    for (let i = 0; i < tableRows.length; i++){
      const name = tableRows[i].getElementsByTagName("td")[0];
      const id = tableRows[i].getElementsByTagName("td")[1];
      if (name && id) {
        const nameString = name.textContent || name.innerText;
        const idString = id.textContent || id.innerText;
        if (nameString.toLowerCase().indexOf(query) > -1 || idString.toLowerCase().indexOf(query) > -1)  {
          tableRows[i].style.display = "";
        } else {
          tableRows[i].style.display = "none";
        }
      }
    }
  }

  refreshSlider() {
    this.graphMethods.highValue = this.dataset.biggest * 0.7;
    this.graphMethods.value = this.dataset.biggest * 0.3;
    this.manualRefresh.emit();
    this.graphMethods.refreshSlider();
  }

  // Toggle the sidebar
  toggleSidebar() {

    this._opened = !this._opened;

  }

  // Go back to the homepage
  hideVis() {
    this.hideVisuals.emit();
  }

  toggleHeatmap() {
    this.heatmap = !this.heatmap;
    this.graphMethods.resize(this.graph, this.heatmap, this.swapped);
    this.heatmapMethods.resize(this.heatmap, this.graph, this.swapped);
  }

  redrawDendro() {
    this.graphMethods.redrawDendro();
  }

  toggleGraph() {
    this.graph = !this.graph;
    this.graphMethods.resize(this.graph, this.heatmap, this.swapped);
    this.heatmapMethods.resize(this.heatmap, this.graph, this.swapped);

    if (this.graph) {
      const self = this;
      setTimeout(function(){ self.graphMethods.refreshReshow(); self.graphMethods.refreshReshow();}, 1);
    }
  }

  switchMode() {
    this.heatmapMethods.grayscale = this.currentMode === 'grayscale';

    this.heatmapMethods.redraw();
    this.currentModeBool = !this.currentModeBool;
  }

  updateLayout() {
    this.heatmapMethods.updateLayout();
  }

  updateSelected(event) {
    this.selectedData = event;
    this.datasetsize = Object.keys(this.selectedData).length;
    // Unselect if length of event is 0
    if (event.length === 0) {
      this.selected = false;
    // If only a node is selected
    } else if (event != null && event[0].to === -1 && event[0].weight === -1) {
      this.fromNode = this.dataset.data[event[0].from].name + ' (' + event[0].from + ')';
      this.cluster = this.dataset.data[event[0].from].cluster;
      this.degree = this.dataset.data[event[0].from].outdegree + this.dataset.data[event[0].from].outdegree;
      this.toNode = null;
      this.weight = null;
      this.selected = true;
      this.singleNode = true;
      this.singleEdge = false;
    // If only one edge is selected
    } else if (event != null && event.length === 1) {
      this.fromNode = this.dataset.data[event[0].from].name + ' (' + event[0].from + ')';
      this.toNode = this.dataset.data[event[0].to].name + ' (' + event[0].to + ')';
      this.weight = event[0].weight;
      this.cluster = null;
      this.degree = null;
      this.selected = true;
      this.singleNode = false;
      this.singleEdge = true;
    // If multiple edges are selected
    } else if (event != null) {
      // this.fromNode = '';
      // this.toNode = '';
      // this.weight = '';
      // If less than edgeLimit edges are selected, list them all
      // const edgeLimit = 30;
      // if (event.length < edgeLimit) {
      //   for (let i = 0; i < event.length; i++) {
          this.fromNode = this.dataset.data[event[0].from].name + ' (' + event[0].from + ')';
          this.toNode = this.dataset.data[event[0].to].name + ' (' + event[0].to + ')';
          this.weight = event[this.counter].weight;
      this.singleNode = false;
      this.singleEdge = false;
      this.graphMethods.selectEdge(event[0].from, event[0].to);
      this.counter = 0;
          // this.fromNode += event[this.counter].from + ', ';
          // this.toNode += event[this.counter].to + ', ';
          // this.weight += event[this.counter].weight + ', ';
        // }
      // Other wise list only 30
      // } else {
        // for (let i = 0; i < edgeLimit; i++) {
      //     this.fromNode += event[i].from + ', ';
      //     this.toNode += event[i].to + ', ';
      //     this.weight += event[i].weight + ', ';
        // }
        // And append dots to show theres more
        // this.fromNode += '...';
        // this.toNode += '...';
        // this.weight += '...';
      // }
      this.cluster = null;
      this.degree = null;
      this.selected = true;
    // If nothing is selected
    } else {
      this.selected = false;
    }
  }

  redrawCircle() {
    this.graphMethods.redrawCircle();
  }

  redrawRandom() {
    this.graphMethods.redrawRandom();
  }

  redrawArch() {
    this.graphMethods.redrawArch();
  }

  onLarge(event) {
    this.graphMethods.largeColor = event.color;
    this.graphMethods.recolor(2);
  }
  onHuge(event) {
    this.graphMethods.hugeColor = event.color;
    this.graphMethods.recolor(1);
  }
  onMedium(event) {
    this.graphMethods.mediumColor = event.color;
    this.graphMethods.recolor(3);
  }
  onSmall(event) {
    this.graphMethods.smallColor = event.color;
    this.graphMethods.recolor(4);
  }
  onTiny(event) {
    this.graphMethods.tinyColor = event.color;
    this.graphMethods.recolor(5);
  }

  hugeHeatmap (data) {
    this.heatmapMethods.hugeColorArray = this.hexToRGBarray(data);
    this.heatmapMethods.redraw();
  }
  largeHeatmap (data) {
    this.heatmapMethods.largeColorArray = this.hexToRGBarray(data);
    this.heatmapMethods.redraw();
  }
  mediumHeatmap (data) {
    this.heatmapMethods.mediumColorArray = this.hexToRGBarray(data);
    this.heatmapMethods.redraw();
  }
  smallHeatmap (data) {
    this.heatmapMethods.smallColorArray = this.hexToRGBarray(data);
    this.heatmapMethods.redraw();
  }
  tinyHeatmap (data) {
    this.heatmapMethods.tinyColorArray = this.hexToRGBarray(data);
    this.heatmapMethods.redraw();
  }
  setBackground(data) {
    this.heatmapMethods.backGroundColor = data.color;
    this.heatmapMethods.redraw();
  }

  hexToRGBarray(data) {
    const rgbArray = [];

    rgbArray[0] = parseInt(data.color[1] + data.color[2], 16);
    rgbArray[1] = parseInt(data.color[3] + data.color[4], 16);
    rgbArray[2] = parseInt(data.color[5] + data.color[6], 16);

    return rgbArray;
  }

  clusterGraph() {
    this.graphMethods.toggleCluster(this.clusterStatus);
    document.getElementById('clusterButt').innerHTML = this.clusterStatus ? '  Cluster' : 'Uncluster'
    this.clusterStatus = !this.clusterStatus;
  }

  startForce() {
    this.sandboxSettings = {
      itRen: parseInt((<HTMLInputElement>document.getElementById('iterRender')).value, 10),
      edgeWeight: parseInt((<HTMLInputElement>document.getElementById('edgeWeight')).value, 10) / 100,
      outAttDist: (<HTMLInputElement>document.getElementById('outAttDist')).checked,
      linLogMode: (<HTMLInputElement>document.getElementById('linLogMode')).checked
    };
    this.force = !this.force;
    document.getElementById('forceButt').innerHTML = (this.force) ? 'Stop Force' : 'Start Force';

    // Timeout needed because otherwise data is not set yet
    const self = this;
    setTimeout(function() { self.graphMethods.toggleForce(); }, 100);

  }

  snapShot() {
    this.graphMethods.makeSnapshot();
  }

  snapShotHeatmap(event) {
    this.heatmapMethods.makeSnapshot(event);
  }

  onEventLogSel(data: any) {
    const rgbArray = this.hexToRGBarray(data);

    this.graphMethods._colorArraySel = this.heatmapMethods._colorArraySel = rgbArray;
    this.graphMethods._colorHex = data.color;
  }

  onEventLogHeat(data: any) {
    this.colorArray = this.hexToRGBarray(data);
  }

  datasetSelected(event) {
    this.dataset = event;
    this.hasHierarchy = typeof this.dataset.hierarchy !== "undefined";
    this.hasCluster = typeof this.dataset.clusterSizes !== "undefined";

    // --------------------------- Change slider based on dataset --------------------------

    const newOptions: Options = Object.assign({}, this.options);
    newOptions.ceil = this.dataset.biggest;

    if (this.dataset.biggest <= 1) {
      newOptions.step = 0.01;
    } else {
      newOptions.step = 1;
    }

    this.options = newOptions;

    this.value = this.dataset.biggest * 0.3;
    this.highValue = this.dataset.biggest * 0.7;

    // Clear and Update Search table
    const table = <HTMLTableElement> document.getElementById('dataTable');

    for (let i = table.rows.length - 1; i > 0; i--) {
      table.deleteRow(i);
    }



    for (let i = 0; i < this.dataset.data.length; i++) {
      let td;
      // Get the node
      const item = this.dataset.data[i];
      // Create the row
      const row = document.createElement("TR");

      // Create the table cell with name
      td = document.createElement("TD");
      td.setAttribute("style", "max-width:60%; white-space:nowrap; overflow:hidden");
      td.setAttribute("id", "row" + item.nodeId.toString());
      const name = document.createTextNode(typeof item.name !== "undefined" ? item.name : "-");
      td.appendChild(name);
      row.appendChild(td);
      // Create the table cell with id
      td = document.createElement("TD");
      const id = document.createTextNode(item.nodeId);
      td.appendChild(id);
      row.appendChild(td);
      // Create the table cell with cluster
      td = document.createElement("TD");
      const cluster = document.createTextNode(typeof item.cluster !== "undefined" ? item.cluster.toString() : "-");
      td.appendChild(cluster);
      row.appendChild(td);
      // Create the table cell with degree
      td = document.createElement("TD");
      const degree = document.createTextNode((item.indegree + item.outdegree).toString());
      td.appendChild(degree);
      row.appendChild(td);
      table.appendChild(row);
    }



  }

  // ----------------- Slider input changes -------------------

  onUserChangeEnd(changeContext: ChangeContext): void {
    this.graphMethods.value = this.value;
    this.graphMethods.highValue = this.highValue;
    this.graphMethods.updateSelected();
  }
}
