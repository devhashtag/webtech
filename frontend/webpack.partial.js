const webpack = require('webpack');

module.exports = {
   plugins: [
     new webpack.ProvidePlugin({
       _sigma: 'sigma'
     })
   ],
   module: {
     rules: [
       {
         test: /sigma.*/,
         use: 'imports-loader?this=>window',
       }
     ]
   }
};
